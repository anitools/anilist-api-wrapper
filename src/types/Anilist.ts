import { type AuthorizationError } from "../utils/AuthorizationError.ts"
import { ResponseError } from "../utils/ResponseError.ts"
import { Mutation, Query } from "../../graphql-codegen/generated/graphql.ts"

type Primitive = string | Function | number | boolean | Symbol | undefined | null

/** Deeply omit members of an array of interface or array of type. */
export type DeepOmitArray<T extends any[], K> = {
  [P in keyof T]: DeepOmit<T[P], K>
}

/**
 * Deeply omit members of an interface or type.
 * https://gist.github.com/ahuggins-nhs/826906a58e4c1e59306bc0792e7826d1?permalink_comment_id=4498424#gistcomment-4498424
 */
type DeepOmit<T, K> = T extends Primitive ? T : {
  [P in Exclude<keyof T, K>]: //extra level of indirection needed to trigger homomorhic behavior
    T[P] extends infer TP // distribute over unions
      ? TP extends Primitive ? TP // leave primitives and functions alone
      : TP extends any[] ? DeepOmitArray<TP, K> // Array special handling
      : DeepOmit<TP, K>
      : never
}

export type Fields<in Q extends (...args: any) => any> = {
  operation: ReturnType<Q>
  level: number
}
export type Fn<F extends (...args: any) => any> = (
  fields: ReturnType<F>,
) => void

export type Variables = { [arg: string]: string | number | boolean | string[] }

export type OperationParser = (root: "query" | "mutation") => {
  get(): string
  set(opts: {
    subField: string
    level: number
    variables?: any
    hasSubField?: true
    isUnion?: true
    alias?: string
  }): ReturnType<OperationParser>
}

export type Authorization = {
  token_type: string
  expires_in: number
  access_token: string
  /** Currently not used for anything */
  refresh_token: string
}

export type Fetch = {
  (
    init:
      & Record<
        | "code"
        | "client_id"
        | "client_secret"
        | "redirect_uri",
        string
      >
      & { "grant_type"?: "authorization_code" },
  ): Promise<Authorization | AuthorizationError>
  (
    init: { query: string; token?: string },
  ): Promise<Response | ResponseError>
}
export type Response = {
  data: DeepOmit<Query & Mutation, "__typename">
}
