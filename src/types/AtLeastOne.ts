export type AtLeastOne<O> = {
  [P in keyof O]:
    & Required<{ [L in P]: O[L] }>
    & { [L in Exclude<keyof O, P>]?: O[L] }
}[keyof O]
