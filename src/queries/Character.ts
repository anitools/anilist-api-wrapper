import {
  CharacterDescriptionArgs,
  CharacterEdgeVoiceActorRolesArgs,
  CharacterEdgeVoiceActorsArgs,
  CharacterMediaArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { PageInfo } from "./Page.ts"
import { FuzzyDate } from "../anilist.ts"
import { Media, MediaConnection } from "./Media.ts"
import { Staff, StaffRoleType } from "./Staff.ts"
import { AtLeastOne } from "../types/AtLeastOne.ts"

const CharacterName = ({ operation, level }: Fields<OperationParser>) => ({
  /** The person's given name */
  first(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "first", level })
    return this
  },
  /** The person's middle name */
  middle(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "middle", level })
    return this
  },
  /** The person's surname */
  last(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "last", level })
    return this
  },
  /** The person's first and last name */
  full(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "full", level })
    return this
  },
  /** The person's full name in their native language */
  native(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "native", level })
    return this
  },
  /** Other names the staff member might be referred to as (pen names) */
  alternative(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "alternative", level })
    return this
  },
  /** The currently authenticated users preferred name language. Default romaji for non-authenticated */
  userPreferred(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userPreferred", level })
    return this
  },
})

const CharacterImage = ({ operation, level }: Fields<OperationParser>) => ({
  /** The person's image of media at its largest size */
  large(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "large", level })
    return this
  },
  /** The person's image of media at medium size */
  medium(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "medium", level })
    return this
  },
})

const CharacterEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op: { alias?: string; fn: Fn<typeof Character> }) {
    operation.set({ alias: op.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Character({ operation, level: level + 1 }))
    else Character({ operation, level: level + 1 }).id()
    return this
  },
  /** The id of the connection */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The characters role in the media */
  role(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "role", level })
    return this
  },
  /** Media specific character name */
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
  /** The voice actors of the character */
  voiceActors(
    op?: { alias?: string; args?: AtLeastOne<CharacterEdgeVoiceActorsArgs>; fn?: Fn<typeof Staff> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "voiceActors",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  /** The voice actors of the character with role date */
  voiceActorRoles(
    op: {
      alias?: string
      args?: AtLeastOne<CharacterEdgeVoiceActorRolesArgs>
      fn: Fn<typeof StaffRoleType>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "voiceActorRoles",
      level,
      variables: op.args,
      hasSubField: true,
    })
    op.fn(StaffRoleType({ operation, level: level + 1 }))
    return this
  },
  /** The media the character is in */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  /** The order the character should be displayed from the users favourites */
  favouriteOrder(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favouriteOrder", level })
    return this
  },
})

export const CharacterConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof CharacterEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(CharacterEdge({ operation, level: level + 1 }))
    else CharacterEdge({ operation, level: level + 1 }).id()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof Character> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(Character({ operation, level: level + 1 }))
    else Character({ operation, level: level + 1 }).id()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const Character = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the character */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The names of the character */
  name(op?: { alias?: string; fn?: Fn<typeof CharacterName> }) {
    operation.set({ alias: op?.alias, subField: "name", level, hasSubField: true })
    if (op?.fn) op.fn(CharacterName({ operation, level: level + 1 }))
    else CharacterName({ operation, level: level + 1 }).userPreferred()
    return this
  },
  /** Character images */
  image(op?: { alias?: string; fn?: Fn<typeof CharacterImage> }) {
    operation.set({ alias: op?.alias, subField: "image", level, hasSubField: true })
    if (op?.fn) op.fn(CharacterImage({ operation, level: level + 1 }))
    else CharacterImage({ operation, level: level + 1 }).medium()
    return this
  },
  /** A general description of the character */
  description(op?: { alias?: string; args?: CharacterDescriptionArgs }) {
    operation.set({ alias: op?.alias, subField: "description", level, variables: op?.args })
    return this
  },
  /** The character's gender. Usually Male, Female, or Non-binary but can be any string. */
  gender(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "gender", level })
    return this
  },
  /** The character's birth date */
  dateOfBirth(op?: { alias?: string; fn?: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op?.alias, subField: "dateOfBirth", level, hasSubField: true })
    if (op?.fn) op.fn(FuzzyDate({ operation, level: level + 1 }))
    else FuzzyDate({ operation, level: level + 1 }).year().month().day()
    return this
  },
  /** The character's age. Note this is a string, not an int, it may contain further text and additional ages. */
  age(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "age", level })
    return this
  },
  /** The characters blood type */
  bloodType(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "bloodType", level })
    return this
  },
  /** If the character is marked as favourite by the currently authenticated user */
  isFavourite(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavourite", level })
    return this
  },
  /** If the character is blocked from being added to favourites */
  isFavouriteBlocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavouriteBlocked", level })
    return this
  },
  /** The url for the character page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** Media that includes the character */
  media(
    op: { alias?: string; args?: AtLeastOne<CharacterMediaArgs>; fn: Fn<typeof MediaConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "media",
      level,
      variables: op.args,
      hasSubField: true,
    })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** The amount of user's who have favourited the character */
  favourites(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favourites", level })
    return this
  },
  /** Notes for site moderators */
  modNotes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "modNotes", level })
    return this
  },
})
