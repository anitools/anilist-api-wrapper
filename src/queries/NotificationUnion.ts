import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { ActivityUnion, MessageActivity } from "./ActivityUnion.ts"
import { Media } from "./Media.ts"
import { Thread } from "./Thread.ts"
import { ThreadComment } from "./ThreadComment.ts"
import { User } from "./User.ts"

const AiringNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the aired anime */
  animeId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "animeId", level })
    return this
  },
  /** The episode number that just aired */
  episode(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "episode", level })
    return this
  },
  /** The notification context text */
  contexts(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "contexts", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The associated media of the airing schedule */
  media(op: { alias?: string; fn: Fn<typeof Media> }) {
    operation.set({ alias: op.alias, subField: "media", level, hasSubField: true })
    op.fn(Media({ operation, level: level + 1 }))
    return this
  },
})

const FollowingNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The id of the user who followed the authenticated user */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The notification context text */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The liked activity */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

const ActivityMessageNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The if of the user who send the message */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the activity message */
  activityId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "activityId", level })
    return this
  },
  /** The notification context text */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The message activity */
  message(op?: { alias?: string; fn?: Fn<typeof MessageActivity> }) {
    operation.set({ alias: op?.alias, subField: "message", level, hasSubField: true })
    if (op?.fn) op.fn(MessageActivity({ operation, level: level + 1 }))
    else MessageActivity({ operation, level: level + 1 }).id()
    return this
  },
  /** The user who sent the message */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

const ActivityNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The id of the user who mentioned the authenticated user */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the activity where mentioned */
  activityId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "activityId", level })
    return this
  },
  /** The notification context text */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The liked activity */
  activity(op?: { alias?: string; fn?: Fn<typeof ActivityUnion> }) {
    operation.set({ alias: op?.alias, subField: "activity", level, hasSubField: true })
    if (op?.fn) op.fn(ActivityUnion({ operation, level: level + 1 }))
    else {
      ActivityUnion({ operation, level: level + 1 })
        .listActivity()
        .messageActivity()
        .textActivity()
    }
    return this
  },
  /** The user who mentioned the authenticated user */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

const ThreadCommentNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The id of the user who mentioned the authenticated user */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the comment where mentioned */
  commentId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "commentId", level })
    return this
  },
  /** The notification context text */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The thread that the relevant comment belongs to */
  thread(op?: { alias?: string; fn?: Fn<typeof Thread> }) {
    operation.set({ alias: op?.alias, subField: "thread", level, hasSubField: true })
    if (op?.fn) op.fn(Thread({ operation, level: level + 1 }))
    else Thread({ operation, level: level + 1 }).id()
    return this
  },
  /** The thread comment that included the @ mention */
  comment(op?: { alias?: string; fn?: Fn<typeof ThreadComment> }) {
    operation.set({ alias: op?.alias, subField: "comment", level, hasSubField: true })
    if (op?.fn) op.fn(ThreadComment({ operation, level: level + 1 }))
    else ThreadComment({ operation, level: level + 1 }).id()
    return this
  },
  /** The user who mentioned the authenticated user */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

const ThreadLikeNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The id of the user who liked to the activity */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the thread which was liked */
  threadId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "threadId", level })
    return this
  },
  /** The notification context text */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The thread that the relevant comment belongs to */
  thread(op?: { alias?: string; fn?: Fn<typeof Thread> }) {
    operation.set({ alias: op?.alias, subField: "thread", level, hasSubField: true })
    if (op?.fn) op.fn(Thread({ operation, level: level + 1 }))
    else Thread({ operation, level: level + 1 }).id()
    return this
  },
  /** The liked thread comment */
  comment(op?: { alias?: string; fn?: Fn<typeof ThreadComment> }) {
    operation.set({ alias: op?.alias, subField: "comment", level, hasSubField: true })
    if (op?.fn) op.fn(ThreadComment({ operation, level: level + 1 }))
    else ThreadComment({ operation, level: level + 1 }).id()
    return this
  },
  /** The user who liked the activity */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

const RelatedMediaAdditionNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the new media */
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaId", level })
    return this
  },
  /** The notification context text */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The associated media of the airing schedule */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})

const MediaDataChangeNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the media that received data changes */
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaId", level })
    return this
  },
  /** The reason for the media data change */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The reason for the media data change */
  reason(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "reason", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The media that received data changes */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})

const MediaMergeNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The id of the media that was merged into */
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaId", level })
    return this
  },
  /** The title of the deleted media */
  deletedMediaTitles(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "deletedMediaTitles", level })
    return this
  },
  /** The reason for the media data change */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The reason for the media merge */
  reason(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "reason", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The media that was merged into */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})

const MediaDeletionNotification = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the Notification */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The type of notification */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The title of the deleted media */
  deletedMediaTitle(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "deletedMediaTitle", level })
    return this
  },
  /** The reason for the media deletion */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
  /** The reason for the media deletion */
  reason(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "reason", level })
    return this
  },
  /** The time the notification was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
})

export const NotificationUnion = ({ operation, level }: Fields<OperationParser>) => ({
  AiringNotification(op?: { alias?: string; fn?: Fn<typeof AiringNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "AiringNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(AiringNotification({ operation, level: level + 1 }))
    else AiringNotification({ operation, level: level + 1 }).id()
    return this
  },
  FollowingNotification(op?: { alias?: string; fn?: Fn<typeof FollowingNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "FollowingNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(FollowingNotification({ operation, level: level + 1 }))
    else FollowingNotification({ operation, level: level + 1 }).id()
    return this
  },
  ActivityMessageNotification(
    op?: { alias?: string; fn?: Fn<typeof ActivityMessageNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityMessageNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) {
      op.fn(ActivityMessageNotification({ operation, level: level + 1 }))
    } else ActivityMessageNotification({ operation, level: level + 1 }).id()
    return this
  },
  ActivityMentionNotification(op?: { alias?: string; fn?: Fn<typeof ActivityNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityMentionNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ActivityNotification({ operation, level: level + 1 }))
    else ActivityNotification({ operation, level: level + 1 }).id()
    return this
  },
  ActivityReplyNotification(op?: { alias?: string; fn?: Fn<typeof ActivityNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityReplyNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ActivityNotification({ operation, level: level + 1 }))
    else ActivityNotification({ operation, level: level + 1 }).id()
    return this
  },
  ActivityReplySubscribedNotification(
    op?: { alias?: string; fn?: Fn<typeof ActivityNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityReplySubscribedNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ActivityNotification({ operation, level: level + 1 }))
    else ActivityNotification({ operation, level: level + 1 }).id()
    return this
  },
  ActivityLikeNotification(op?: { alias?: string; fn?: Fn<typeof ActivityNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityLikeNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ActivityNotification({ operation, level: level + 1 }))
    else ActivityNotification({ operation, level: level + 1 }).id()
    return this
  },
  ActivityReplyLikeNotification(op?: { alias?: string; fn?: Fn<typeof ActivityNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityReplyLikeNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ActivityNotification({ operation, level: level + 1 }))
    else ActivityNotification({ operation, level: level + 1 }).id()
    return this
  },
  ThreadCommentMentionNotification(
    op?: { alias?: string; fn?: Fn<typeof ThreadCommentNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "ThreadCommentMentionNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ThreadCommentNotification({ operation, level: level + 1 }))
    else ThreadCommentNotification({ operation, level: level + 1 }).id()
    return this
  },
  ThreadCommentReplyNotification(
    op?: { alias?: string; fn?: Fn<typeof ThreadCommentNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "ThreadCommentReplyNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ThreadCommentNotification({ operation, level: level + 1 }))
    else ThreadCommentNotification({ operation, level: level + 1 }).id()
    return this
  },
  ThreadCommentSubscribedNotification(
    op?: { alias?: string; fn?: Fn<typeof ThreadCommentNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "ThreadCommentSubscribedNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ThreadCommentNotification({ operation, level: level + 1 }))
    else ThreadCommentNotification({ operation, level: level + 1 }).id()
    return this
  },
  ThreadCommentLikeNotification(
    op?: { alias?: string; fn?: Fn<typeof ThreadCommentNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "ThreadCommentLikeNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ThreadCommentNotification({ operation, level: level + 1 }))
    else ThreadCommentNotification({ operation, level: level + 1 }).id()
    return this
  },
  ThreadLikeNotification(op?: { alias?: string; fn?: Fn<typeof ThreadLikeNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "ThreadLikeNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ThreadLikeNotification({ operation, level: level + 1 }))
    else ThreadLikeNotification({ operation, level: level + 1 }).id()
    return this
  },
  RelatedMediaAdditionNotification(
    op?: { alias?: string; fn?: Fn<typeof RelatedMediaAdditionNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "RelatedMediaAdditionNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) {
      op.fn(RelatedMediaAdditionNotification({ operation, level: level + 1 }))
    } else {RelatedMediaAdditionNotification({ operation, level: level + 1 })
        .id()}
    return this
  },
  MediaDataChangeNotification(
    op?: { alias?: string; fn?: Fn<typeof MediaDataChangeNotification> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "MediaDataChangeNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) {
      op.fn(MediaDataChangeNotification({ operation, level: level + 1 }))
    } else MediaDataChangeNotification({ operation, level: level + 1 }).id()
    return this
  },
  MediaMergeNotification(op?: { alias?: string; fn?: Fn<typeof MediaMergeNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "MediaMergeNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(MediaMergeNotification({ operation, level: level + 1 }))
    else MediaMergeNotification({ operation, level: level + 1 }).id()
    return this
  },
  MediaDeletionNotification(op?: { alias?: string; fn?: Fn<typeof MediaDeletionNotification> }) {
    operation.set({
      alias: op?.alias,
      subField: "MediaDeletionNotification",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(MediaDeletionNotification({ operation, level: level + 1 }))
    else MediaDeletionNotification({ operation, level: level + 1 }).id()
    return this
  },
})
