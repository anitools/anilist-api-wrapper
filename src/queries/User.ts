import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { UserFavouritesArgs } from "../../graphql-codegen/generated/graphql.ts"
import { Favourites } from "./Favourites.ts"
import { MediaListOptions } from "./MediaList.ts"
import { UserStatisticTypes } from "./UserStatistics.ts"

const NotificationOption = ({ operation, level }: Fields<OperationParser>) => ({
  enabled(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "enabled", level })
    return this
  },
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
})

const ListActivityOption = ({ operation, level }: Fields<OperationParser>) => ({
  disabled(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "disabled", level })
    return this
  },
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
})

export const UserPreviousName = ({ operation, level }: Fields<OperationParser>) => ({
  /** A previous name of the user. */
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
  /** When the user first changed from this name. */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** When the user most recently changed from this name. */
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedAt", level })
    return this
  },
})

export const UserOptions = ({ operation, level }: Fields<OperationParser>) => ({
  /** The language the user wants to see media titles in */
  titleLanguage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "titleLanguage", level })
    return this
  },

  /** Whether the user has enabled viewing of 18+ content */
  displayAdultContent(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "displayAdultContent", level })
    return this
  },

  /** Whether the user receives notifications when a show they are watching aires */
  airingNotifications(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "airingNotifications", level })
    return this
  },

  /** Profile highlight color (blue, purple, pink, orange, red, green, gray) */
  profileColor(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "profileColor", level })
    return this
  },

  /** Notification options */
  notificationOptions(op: { alias?: string; fn: Fn<typeof NotificationOption> }) {
    operation.set({ alias: op.alias, subField: "notificationOptions", level, hasSubField: true })
    op.fn(NotificationOption({ operation, level: level + 1 }))
    return this
  },

  /** The user's timezone offset (Auth user only) */
  timezone(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "timezone", level })
    return this
  },

  /** Minutes between activity for them to be merged together. 0 is Never, Above 2 weeks (20160 mins) is Always. */
  activityMergeTime(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "activityMergeTime", level })
    return this
  },

  /** The language the user wants to see staff and character names in */
  staffNameLanguage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "staffNameLanguage", level })
    return this
  },

  /** Whether the user only allow messages from users they follow */
  restrictMessagesToFollowing(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "restrictMessagesToFollowing", level })
    return this
  },

  /** The list activity types the user has disabled from being created from list updates */
  disabledListActivity(op: { alias?: string; fn: Fn<typeof ListActivityOption> }) {
    operation.set({ alias: op.alias, subField: "disabledListActivity", level, hasSubField: true })
    op.fn(ListActivityOption({ operation, level: level + 1 }))
    return this
  },
})

export const UserAvatar = ({ operation, level }: Fields<OperationParser>) => ({
  /** The avatar of user at its largest size */
  large(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "large", level })
    return this
  },
  /** The avatar of user at medium size */
  medium(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "medium", level })
    return this
  },
})

export const User = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the user */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The name of the user */
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
  /** The bio written by user (Markdown) */
  about(op?: { alias?: string; args?: { asHtml: boolean } }) {
    operation.set({ alias: op?.alias, subField: "about", level, variables: op?.args })
    return this
  },
  /** The user's avatar images */
  avatar(op?: { alias?: string; fn?: Fn<typeof UserAvatar> }) {
    operation.set({ alias: op?.alias, subField: "avatar", level, hasSubField: true })
    if (op?.fn) op.fn(UserAvatar({ operation, level: level + 1 }))
    else UserAvatar({ operation, level: level + 1 }).medium()
    return this
  },
  /** The user's banner images */
  bannerImage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "bannerImage", level })
    return this
  },
  /** If the authenticated user if following this user */
  isFollowing(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFollowing", level })
    return this
  },
  /** If this user if following the authenticated user */
  isFollower(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFollower", level })
    return this
  },
  /** If the user is blocked by the authenticated user */
  isBlocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isBlocked", level })
    return this
  },
  bans(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "bans", level })
    return this
  },
  /* The user's general options */
  options(op: { alias?: string; fn: Fn<typeof UserOptions> }) {
    operation.set({ alias: op.alias, subField: "options", level, hasSubField: true })
    op.fn(UserOptions({ operation, level: level + 1 }))
    return this
  },
  /* The user's media list options */
  mediaListOptions(op: { alias?: string; fn: Fn<typeof MediaListOptions> }) {
    operation.set({ alias: op.alias, subField: "mediaListOptions", level, hasSubField: true })
    op.fn(MediaListOptions({ operation, level: level + 1 }))
    return this
  },
  /* The users favourites */
  favourites(op: { alias?: string; args?: UserFavouritesArgs; fn: Fn<typeof Favourites> }) {
    operation.set({
      alias: op.alias,
      subField: "favourites",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(Favourites({ operation, level: level + 1 }))
    return this
  },
  /* The users anime & manga list statistics */
  statistics(op: { alias?: string; fn: Fn<typeof UserStatisticTypes> }) {
    operation.set({ alias: op.alias, subField: "statistics", level, hasSubField: true })
    op.fn(UserStatisticTypes({ operation, level: level + 1 }))
    return this
  },
  /* The number of unread notifications the user has */
  unreadNotificationCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "unreadNotificationCount", level })
    return this
  },
  /* The url for the user page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /* The donation tier of the user */
  donatorTier(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "donatorTier", level })
    return this
  },
  /* Custom donation badge text */
  donatorBadge(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "donatorBadge", level })
    return this
  },
  /* The user's moderator roles if they are a site moderator */
  moderatorRoles(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "moderatorRoles", level })
    return this
  },
  /* When the user's account was created. (Does not exist for accounts created before 2020) */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /* When the user's data was last updated */
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedAt", level })
    return this
  },
  /* The user's previously used names. */
  previousNames(op?: { alias?: string; fn?: Fn<typeof UserPreviousName> }) {
    operation.set({ alias: op?.alias, subField: "previousNames", level, hasSubField: true })
    if (op?.fn) op.fn(UserPreviousName({ operation, level: level + 1 }))
    else UserPreviousName({ operation, level: level + 1 }).name()
    return this
  },
})
