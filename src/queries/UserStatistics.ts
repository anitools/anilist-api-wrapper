import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import {
  UserStatisticsCountriesArgs,
  UserStatisticsFormatsArgs,
  UserStatisticsGenresArgs,
  UserStatisticsLengthsArgs,
  UserStatisticsReleaseYearsArgs,
  UserStatisticsScoresArgs,
  UserStatisticsStaffArgs,
  UserStatisticsStartYearsArgs,
  UserStatisticsStatusesArgs,
  UserStatisticsStudiosArgs,
  UserStatisticsTagsArgs,
  UserStatisticsVoiceActorsArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { Staff } from "./Staff.ts"
import { Studio } from "./Studio.ts"

const UserFormatStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  format(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "format", level })
    return this
  },
})

const UserStatusStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  status(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "status", level })
    return this
  },
})

const UserScoreStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  score(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "score", level })
    return this
  },
})

const UserLengthStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  length(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "length", level })
    return this
  },
})

const UserReleaseYearStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  releaseYear(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "releaseYear", level })
    return this
  },
})

const UserStartYearStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  startYear(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "startYear", level })
    return this
  },
})

const UserGenreStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  genre(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "genre", level })
    return this
  },
})

const UserTagStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  tag(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "tag", level })
    return this
  },
})

const UserCountryStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  country(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "country", level })
    return this
  },
})

const UserVoiceActorStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  voiceActor(op?: { alias?: string; fn?: Fn<typeof Staff> }) {
    operation.set({ alias: op?.alias, subField: "voiceActor", level, hasSubField: true })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  characterIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "characterIds", level })
    return this
  },
})

const UserStaffStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  staff(op?: { alias?: string; fn?: Fn<typeof Staff> }) {
    operation.set({ alias: op?.alias, subField: "staff", level, hasSubField: true })
    if (op?.fn) op?.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
})

const UserStudioStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  mediaIds(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaIds", level })
    return this
  },
  studio(op?: { alias?: string; fn?: Fn<typeof Studio> }) {
    operation.set({ alias: op?.alias, subField: "studio", level, hasSubField: true })
    if (op?.fn) op.fn(Studio({ operation, level: level + 1 }))
    else Studio({ operation, level: level + 1 }).id()
    return this
  },
})

export const UserStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  standardDeviation(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "standardDeviation", level })
    return this
  },
  minutesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "minutesWatched", level })
    return this
  },
  episodesWatched(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "episodesWatched", level })
    return this
  },
  chaptersRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chaptersRead", level })
    return this
  },
  volumesRead(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "volumesRead", level })
    return this
  },
  formats(
    op: { alias?: string; args?: UserStatisticsFormatsArgs; fn: Fn<typeof UserFormatStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "formats", level, hasSubField: true })
    op.fn(UserFormatStatistics({ operation, level: level + 1 }))
    return this
  },
  statuses(
    op: { alias?: string; args?: UserStatisticsStatusesArgs; fn: Fn<typeof UserStatusStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "statuses", level, hasSubField: true })
    op.fn(UserStatusStatistics({ operation, level: level + 1 }))
    return this
  },
  scores(
    op: { alias?: string; args?: UserStatisticsScoresArgs; fn: Fn<typeof UserScoreStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "scores", level, hasSubField: true })
    op.fn(UserScoreStatistics({ operation, level: level + 1 }))
    return this
  },
  lengths(
    op: { alias?: string; args?: UserStatisticsLengthsArgs; fn: Fn<typeof UserLengthStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "lengths", level, hasSubField: true })
    op.fn(UserLengthStatistics({ operation, level: level + 1 }))
    return this
  },
  releaseYears(
    op: {
      alias?: string
      args?: UserStatisticsReleaseYearsArgs
      fn: Fn<typeof UserReleaseYearStatistics>
    },
  ) {
    operation.set({ alias: op.alias, subField: "releaseYears", level, hasSubField: true })
    op.fn(UserReleaseYearStatistics({ operation, level: level + 1 }))
    return this
  },
  startYears(
    op: {
      alias?: string
      args?: UserStatisticsStartYearsArgs
      fn: Fn<typeof UserStartYearStatistics>
    },
  ) {
    operation.set({ alias: op.alias, subField: "startYears", level, hasSubField: true })
    op.fn(UserStartYearStatistics({ operation, level: level + 1 }))
    return this
  },
  genres(
    op: { alias?: string; args?: UserStatisticsGenresArgs; fn: Fn<typeof UserGenreStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "genres", level, hasSubField: true })
    op.fn(UserGenreStatistics({ operation, level: level + 1 }))
    return this
  },
  tags(
    op: { alias?: string; args?: UserStatisticsTagsArgs; fn: Fn<typeof UserTagStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "tags", level, hasSubField: true })
    op.fn(UserTagStatistics({ operation, level: level + 1 }))
    return this
  },
  countries(
    op: {
      alias?: string
      args?: UserStatisticsCountriesArgs
      fn: Fn<typeof UserCountryStatistics>
    },
  ) {
    operation.set({ alias: op.alias, subField: "countries", level, hasSubField: true })
    op.fn(UserCountryStatistics({ operation, level: level + 1 }))
    return this
  },
  voiceActors(
    op: {
      alias?: string
      args?: UserStatisticsVoiceActorsArgs
      fn: Fn<typeof UserVoiceActorStatistics>
    },
  ) {
    operation.set({ alias: op.alias, subField: "voiceActors", level, hasSubField: true })
    op.fn(UserVoiceActorStatistics({ operation, level: level + 1 }))
    return this
  },
  staff(
    op: { alias?: string; args?: UserStatisticsStaffArgs; fn: Fn<typeof UserStaffStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "staff", level, hasSubField: true })
    op.fn(UserStaffStatistics({ operation, level: level + 1 }))
    return this
  },
  studios(
    op: { alias?: string; args?: UserStatisticsStudiosArgs; fn: Fn<typeof UserStudioStatistics> },
  ) {
    operation.set({ alias: op.alias, subField: "studios", level, hasSubField: true })
    op.fn(UserStudioStatistics({ operation, level: level + 1 }))
    return this
  },
})

export const UserStatisticTypes = ({ operation, level }: Fields<OperationParser>) => ({
  anime(op: { alias?: string; fn: Fn<typeof UserStatistics> }) {
    operation.set({ alias: op.alias, subField: "anime", level, hasSubField: true })
    op.fn(UserStatistics({ operation, level: level + 1 }))
    return this
  },
  manga(op: { alias?: string; fn: Fn<typeof UserStatistics> }) {
    operation.set({ alias: op.alias, subField: "manga", level, hasSubField: true })
    op.fn(UserStatistics({ operation, level: level + 1 }))
    return this
  },
})
