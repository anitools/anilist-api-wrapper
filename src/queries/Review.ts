import { PageInfo } from "./Page.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { Media } from "./Media.ts"
import { User } from "./User.ts"

const ReviewEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op?: { alias?: string; fn?: Fn<typeof Review> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Review({ operation, level: level + 1 }))
    else Review({ operation, level: level + 1 }).id()
    return this
  },
})

export const ReviewConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof ReviewEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(ReviewEdge({ operation, level: level + 1 }))
    else ReviewEdge({ operation, level: level + 1 }).node()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof Review> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(Review({ operation, level: level + 1 }))
    else Review({ operation, level: level + 1 }).id()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const Review = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the review */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The id of the review's creator */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userid", level })
    return this
  },
  /** The id of the review's media */
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaid", level })
    return this
  },
  /** For which type of media the review is for */
  mediaType(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediatype", level })
    return this
  },
  /** A short summary of the review */
  summary(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "summary", level })
    return this
  },
  /** The main review body text */
  body(op?: { alias?: string; args?: { asHtml: boolean } }) {
    operation.set({ alias: op?.alias, subField: "body", level, variables: op?.args })
    return this
  },
  /** The total user rating of the review */
  rating(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "rating", level })
    return this
  },
  /** The amount of user ratings of the review */
  ratingAmount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "ratingamount", level })
    return this
  },
  /** The rating of the review by currently authenticated user */
  userRating(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userrating", level })
    return this
  },
  /** The review score of the media */
  score(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "score", level })
    return this
  },
  /** If the review is not yet publicly published and is only viewable by creator */
  private(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "private", level })
    return this
  },
  /** The url for the review page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteurl", level })
    return this
  },
  /** The time of the thread creation */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdat", level })
    return this
  },
  /** The time of the thread last update */
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedat", level })
    return this
  },
  /** The creator of the review */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The media the review is of */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})
