import { StudioMediaArgs } from "../../graphql-codegen/generated/graphql.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { PageInfo } from "./Page.ts"
import { MediaConnection } from "./Media.ts"
import { AtLeastOne } from "../types/AtLeastOne.ts"

const StudioEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op?: { alias?: string; fn?: Fn<typeof Studio> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Studio({ operation, level: level + 1 }))
    else Studio({ operation, level: level + 1 }).id()
    return this
  },
  /** The id of the connection */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** If the studio is the main animation studio of the anime */
  isMain(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isMain", level })
    return this
  },
  /** The order the character should be displayed from the users favourites */
  favouriteOrder(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favouriteOrder", level })
    return this
  },
})

export const StudioConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof StudioEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(StudioEdge({ operation, level: level + 1 }))
    else StudioEdge({ operation, level: level + 1 }).node()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof Studio> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(Studio({ operation, level: level + 1 }))
    else Studio({ operation, level: level + 1 }).id()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const Studio = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the studio */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The name of the studio */
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
  /** If the studio is an animation studio or a different kind of company */
  isAnimationStudio(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isAnimationStudio", level })
    return this
  },
  /** The media the studio has worked on */
  media(
    op: { alias?: string; args?: AtLeastOne<StudioMediaArgs>; fn: Fn<typeof MediaConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "media",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** The url for the studio page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** If the studio is marked as favourite by the currently authenticated user */
  isFavourite(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavourite", level })
    return this
  },
  /** The amount of user's who have favourited the studio */
  favourites(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favourites", level })
    return this
  },
})
