import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { AtLeastOne } from "../types/AtLeastOne.ts"
import {
  FavouritesAnimeArgs,
  FavouritesCharactersArgs,
  FavouritesMangaArgs,
  FavouritesStaffArgs,
  FavouritesStudiosArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { CharacterConnection } from "./Character.ts"
import { MediaConnection } from "./Media.ts"
import { StaffConnection } from "./Staff.ts"
import { StudioConnection } from "./Studio.ts"

export const Favourites = ({ operation, level }: Fields<OperationParser>) => ({
  /** Favourite anime */
  anime(
    op: { alias?: string; args?: AtLeastOne<FavouritesAnimeArgs>; fn: Fn<typeof MediaConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "anime",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** Favourite manga */
  manga(
    op: { alias?: string; args?: AtLeastOne<FavouritesMangaArgs>; fn: Fn<typeof MediaConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "manga",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** Favourite characters */
  characters(
    op: {
      alias?: string
      args?: AtLeastOne<FavouritesCharactersArgs>
      fn: Fn<typeof CharacterConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "characters",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(CharacterConnection({ operation, level: level + 1 }))
    return this
  },
  /** Favourite staff */
  staff(
    op: { alias?: string; args?: AtLeastOne<FavouritesStaffArgs>; fn: Fn<typeof StaffConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "staff",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(StaffConnection({ operation, level: level + 1 }))
    return this
  },
  /** Favourite studios */
  studios(
    op: {
      alias?: string
      args?: AtLeastOne<FavouritesStudiosArgs>
      fn: Fn<typeof StudioConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "studios",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(StudioConnection({ operation, level: level + 1 }))
    return this
  },
})
