import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { Media } from "./Media.ts"
import { FuzzyDate } from "../anilist.ts"
import { User } from "./User.ts"
import { MediaListScoreArgs } from "../../graphql-codegen/generated/graphql.ts"

export const MediaListTypeOptions = ({ operation, level }: Fields<OperationParser>) => ({
  /** The order each list should be displayed in */
  sectionOrder(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "sectionOrder", level })
    return this
  },
  /** If the completed sections of the list should be separated by format */
  splitCompletedSectionByFormat(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "splitCompletedSectionByFormat", level })
    return this
  },
  /** The names of the user's custom lists */
  customLists(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "customLists", level })
    return this
  },
  /** The names of the user's advanced scoring sections */
  advancedScoring(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "advancedScoring", level })
    return this
  },
  /** If advanced scoring is enabled */
  advancedScoringEnabled(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "advancedScoringEnabled", level })
    return this
  },
})

export const MediaListOptions = ({ operation, level }: Fields<OperationParser>) => ({
  /** The score format the user is using for media lists */
  scoreFormat(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "scoreFormat", level })
    return this
  },
  /** The default order list rows should be displayed in */
  rowOrder(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "rowOrder", level })
    return this
  },
  /** The user's anime list options */
  animeList(op: { alias?: string; fn: Fn<typeof MediaListTypeOptions> }) {
    operation.set({ alias: op.alias, subField: "animeList", level, hasSubField: true })
    op.fn(MediaListTypeOptions({ operation, level: level + 1 }))
    return this
  },
  /** The user's manga list options */
  mangaList(op: { alias?: string; fn: Fn<typeof MediaListTypeOptions> }) {
    operation.set({ alias: op.alias, subField: "mangaList", level, hasSubField: true })
    op.fn(MediaListTypeOptions({ operation, level: level + 1 }))
    return this
  },
})

export const MediaList = ({ operation, level }: Fields<OperationParser>) => ({
  completedAt(op: { alias?: string; fn: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op.alias, subField: "completedAt", level, hasSubField: true })
    op.fn(FuzzyDate({ operation, level: level + 1 }))
    return this
  },
  startedAt(op: { alias?: string; fn: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op.alias, subField: "startedAt", level, hasSubField: true })
    op.fn(FuzzyDate({ operation, level: level + 1 }))
    return this
  },
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaId", level })
    return this
  },
  score(op?: { alias?: string; args?: MediaListScoreArgs }) {
    operation.set({ alias: op?.alias, subField: "score", level, variables: op?.args })
    return this
  },
  status(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "status", level })
    return this
  },
  progress(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "progress", level })
    return this
  },
  progressVolumes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "progressVolumes", level })
    return this
  },
  repeat(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "repeat", level })
    return this
  },
  priority(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "priority", level })
    return this
  },
  private(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "private", level })
    return this
  },
  notes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "notes", level })
    return this
  },
  advancedScores(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "advancedScores", level })
    return this
  },
  hiddenFromStatusLists(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "hiddenFromStatusLists", level })
    return this
  },
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedAt", level })
    return this
  },
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})
