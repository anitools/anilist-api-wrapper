import { Fields, OperationParser } from "../types/Anilist.ts"

export const MediaTag = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the tag */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The name of the tag */
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
  /** A general description of the tag */
  description(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "description", level })
    return this
  },
  /** The categories of tags this tag belongs to */
  category(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "category", level })
    return this
  },
  /** The relevance ranking of the tag out of the 100 for this media */
  rank(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "rank", level })
    return this
  },
  /** If the tag could be a spoiler for any media */
  isGeneralSpoiler(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isGeneralSpoiler", level })
    return this
  },
  /** If the tag is a spoiler for this media */
  isMediaSpoiler(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isMediaSpoiler", level })
    return this
  },
  /** If the tag is only for adult 18+ media */
  isAdult(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isAdult", level })
    return this
  },
  /** The user who submitted the tag */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
})
