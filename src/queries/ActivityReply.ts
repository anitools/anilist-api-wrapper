import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { ActivityReplyTextArgs } from "../../graphql-codegen/generated/graphql.ts"
import { User } from "./User.ts"

export const ActivityReply = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the reply */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The id of the replies creator */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The id of the parent activity */
  activityId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "activityId", level })
    return this
  },
  /** The reply text */
  text(op?: { alias?: string; args?: ActivityReplyTextArgs }) {
    operation.set({ alias: op?.alias, subField: "text", level, variables: op?.args })
    return this
  },
  /** The amount of likes the reply has */
  likeCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "likeCount", level })
    return this
  },
  /** If the currently authenticated user liked the reply */
  isLiked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLiked", level })
    return this
  },
  /** The time the reply was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The user who created reply */
  user(op: { alias?: string; fn: Fn<typeof User> }) {
    operation.set({ alias: op.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The users who liked the reply */
  likes(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "likes", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})
