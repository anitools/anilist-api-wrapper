import {
  SiteStatisticsAnimeArgs,
  SiteStatisticsCharactersArgs,
  SiteStatisticsMangaArgs,
  SiteStatisticsReviewsArgs,
  SiteStatisticsStaffArgs,
  SiteStatisticsStudiosArgs,
  SiteStatisticsUsersArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import type { AtLeastOne } from "../types/AtLeastOne.ts"
import { SiteTrendConnection } from "./SiteTrend.ts"

export const SiteStatistics = ({ operation, level }: Fields<OperationParser>) => ({
  users(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsUsersArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "users",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
  anime(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsAnimeArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "anime",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
  manga(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsMangaArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "manga",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
  characters(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsCharactersArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "characters",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
  staff(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsStaffArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "staff",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
  studios(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsStudiosArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "studios",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
  reviews(
    op: {
      alias?: string
      args?: AtLeastOne<SiteStatisticsReviewsArgs>
      fn: Fn<typeof SiteTrendConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "reviews",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(SiteTrendConnection({ operation, level: level + 1 }))
    return this
  },
})
