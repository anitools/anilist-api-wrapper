import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { Media } from "./Media.ts"
import { User } from "./User.ts"

const ThreadCategory = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the category */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The name of the category */
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
})

export const Thread = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the thread */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The title of the thread */
  title(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "title", level })
    return this
  },
  /** The text body of the thread (Markdown) */
  body(op?: { alias?: string; args?: { asHtml: boolean } }) {
    operation.set({ alias: op?.alias, subField: "body", level, variables: op?.args })
    return this
  },
  /** The id of the thread owner user */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The id of the user who most recently commented on the thread */
  replyUserId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "replyUserId", level })
    return this
  },
  /** The id of the most recent comment on the thread */
  replyCommentId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "replyCommentId", level })
    return this
  },
  /** The number of comments on the thread */
  replyCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "replyCount", level })
    return this
  },
  /** The number of times users have viewed the thread */
  viewCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "viewCount", level })
    return this
  },
  /** If the thread is locked and can receive comments */
  isLocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLocked", level })
    return this
  },
  /** If the thread is stickied and should be displayed at the top of the page */
  isSticky(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isSticky", level })
    return this
  },
  /** If the currently authenticated user is subscribed to the thread */
  isSubscribed(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isSubscribed", level })
    return this
  },
  /** The amount of likes the thread has */
  likeCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "likeCount", level })
    return this
  },
  /** If the currently authenticated user liked the thread */
  isLiked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLiked", level })
    return this
  },
  /** The time of the last reply */
  repliedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "repliedAt", level })
    return this
  },
  /** The time of the thread creation */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The time of the thread last update */
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedAt", level })
    return this
  },
  /** The owner of the thread */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The user to last reply to the thread */
  replyUser(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "replayUser", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The users who liked the thread */
  likes(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "likes", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The url for the thread page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** The categories of the thread */
  categories(op: { alias?: string; fn: Fn<typeof ThreadCategory> }) {
    operation.set({ alias: op.alias, subField: "categories", level, hasSubField: true })
    op.fn(ThreadCategory({ operation, level: level + 1 }))
    return this
  },
  /** The media categories of the thread */
  mediaCategories(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "mediaCategories", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})
