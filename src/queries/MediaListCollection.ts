import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { MediaListGroup } from "./Media.ts"
import { User } from "./User.ts"

export const MediaListCollection = ({ operation, level }: Fields<OperationParser>) => ({
  /** Grouped media list entries */
  lists(op: { alias?: string; fn: Fn<typeof MediaListGroup> }) {
    operation.set({ alias: op.alias, subField: "lists", level, hasSubField: true })
    op.fn(MediaListGroup({ operation, level: level + 1 }))
    return this
  },
  /** The owner of the list */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** If there is another chunk */
  hasNextChunk(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "hasNextChunk", level })
    return this
  },
})
