import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { ActivityReply } from "./ActivityReply.ts"
import { ListActivity, MessageActivity, TextActivity } from "./ActivityUnion.ts"
import { Thread } from "./Thread.ts"
import { ThreadComment } from "./ThreadComment.ts"

export const LikeableUnion = ({ operation, level }: Fields<OperationParser>) => ({
  listActivity(op?: { alias?: string; fn?: Fn<typeof ListActivity> }) {
    operation.set({
      alias: op?.alias,
      subField: "ListActivity",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ListActivity({ operation, level: level + 1 }))
    else ListActivity({ operation, level: level + 1 }).id()
    return this
  },
  textActivity(op?: { alias?: string; fn?: Fn<typeof TextActivity> }) {
    operation.set({
      alias: op?.alias,
      subField: "TextActivity",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(TextActivity({ operation, level: level + 1 }))
    else TextActivity({ operation, level: level + 1 }).id()
    return this
  },
  messageActivity(op?: { alias?: string; fn?: Fn<typeof MessageActivity> }) {
    operation.set({
      alias: op?.alias,
      subField: "MessageActivity",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(MessageActivity({ operation, level: level + 1 }))
    else MessageActivity({ operation, level: level + 1 }).id()
    return this
  },
  activityReply(op?: { alias?: string; fn?: Fn<typeof ActivityReply> }) {
    operation.set({
      alias: op?.alias,
      subField: "ActivityReply",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ActivityReply({ operation, level: level + 1 }))
    else ActivityReply({ operation, level: level + 1 }).id()
    return this
  },
  thread(op?: { alias?: string; fn?: Fn<typeof Thread> }) {
    operation.set({ alias: op?.alias, subField: "Thread", level, hasSubField: true, isUnion: true })
    if (op?.fn) op.fn(Thread({ operation, level: level + 1 }))
    else Thread({ operation, level: level + 1 }).id()
    return this
  },
  threadComment(op?: { alias?: string; fn?: Fn<typeof ThreadComment> }) {
    operation.set({
      alias: op?.alias,
      subField: "ThreadComment",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ThreadComment({ operation, level: level + 1 }))
    else ThreadComment({ operation, level: level + 1 }).id()
    return this
  },
})
