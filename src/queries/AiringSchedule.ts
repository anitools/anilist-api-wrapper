import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { Media } from "./Media.ts"
import { PageInfo } from "./Page.ts"

const AiringScheduleEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op: { alias?: string; fn: Fn<typeof AiringSchedule> }) {
    operation.set({ alias: op.alias, subField: "node", level, hasSubField: true })
    op.fn(AiringSchedule({ operation, level: level + 1 }))
    return this
  },
  /** The id of the connection */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
})

export const AiringScheduleConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op: { alias?: string; fn: Fn<typeof AiringScheduleEdge> }) {
    operation.set({ alias: op.alias, subField: "edges", level, hasSubField: true })
    op.fn(AiringScheduleEdge({ operation, level: level + 1 }))
    return this
  },
  nodes(op: { alias?: string; fn: Fn<typeof AiringSchedule> }) {
    operation.set({ alias: op.alias, subField: "nodes", level, hasSubField: true })
    op.fn(AiringSchedule({ operation, level: level + 1 }))
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const AiringSchedule = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the airing schedule item */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The time the episode airs at */
  airingAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "airingAt", level })
    return this
  },
  /** Seconds until episode starts airing */
  timeUntilAiring(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "timeUntilAiring", level })
    return this
  },
  /** The airing episode number */
  episode(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "episode", level })
    return this
  },
  /** The associate media id of the airing episode */
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaId", level })
    return this
  },
  /** The associate media of the airing episode */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})
