import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { PageInfo } from "./Page.ts"

const SiteTrend = ({ operation, level }: Fields<OperationParser>) => ({
  /** The day the data was recorded (timestamp) */
  date(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "date", level })
    return this
  },

  count(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "count", level })
    return this
  },
  /** The change from yesterday */
  change(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "change", level })
    return this
  },
})

const SiteTrendEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op: { alias?: string; fn: Fn<typeof SiteTrend> }) {
    operation.set({ alias: op.alias, subField: "node", level, hasSubField: true })
    op.fn(SiteTrend({ operation, level: level + 1 }))
    return this
  },
})

export const SiteTrendConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op: { alias?: string; fn: Fn<typeof SiteTrendEdge> }) {
    operation.set({ alias: op.alias, subField: "edges", level, hasSubField: true })
    op.fn(SiteTrendEdge({ operation, level: level + 1 }))
    return this
  },
  nodes(op: { alias?: string; fn: Fn<typeof SiteTrend> }) {
    operation.set({ alias: op.alias, subField: "nodes", level, hasSubField: true })
    op.fn(SiteTrend({ operation, level: level + 1 }))
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})
