import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { Media } from "./Media.ts"
import { User } from "./User.ts"
import { PageInfo } from "./Page.ts"

const RecommendationEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op?: { alias?: string; fn?: Fn<typeof Recommendation> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Recommendation({ operation, level: level + 1 }))
    else Recommendation({ operation, level: level + 1 }).id()
    return this
  },
})
export const RecommendationConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof RecommendationEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(RecommendationEdge({ operation, level: level + 1 }))
    else RecommendationEdge({ operation, level: level + 1 }).node()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof Recommendation> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(Recommendation({ operation, level: level + 1 }))
    else Recommendation({ operation, level: level + 1 }).id()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const Recommendation = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the recommendation */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** Users rating of the recommendation */
  rating(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "rating", level })
    return this
  },
  /** The rating of the recommendation by currently authenticated user */
  userRating(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userRating", level })
    return this
  },
  /** The media the recommendation is from */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  /** The recommended media */
  mediaRecommendation(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  /** The user that first created the recommendation */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})
