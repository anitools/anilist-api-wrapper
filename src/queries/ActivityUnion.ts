import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import {
  MessageActivityMessageArgs,
  TextActivityTextArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { ActivityReply } from "./ActivityReply.ts"
import { Media } from "./Media.ts"
import { User } from "./User.ts"

export const TextActivity = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the activity */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The user id of the activity's creator */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of activity */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The number of activity replies */
  replyCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "replyCount", level })
    return this
  },
  /** The status text (Markdown) */
  text(op?: { alias?: string; args?: TextActivityTextArgs }) {
    operation.set({ alias: op?.alias, subField: "text", level, variables: op?.args })
    return this
  },
  /** The url for the activity page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** If the activity is locked and can receive replies */
  isLocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLocked", level })
    return this
  },
  /** If the currently authenticated user is subscribed to the activity */
  isSubscribed(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isSubscribed", level })
    return this
  },
  /** The amount of likes the activity has */
  likeCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "likeCount", level })
    return this
  },
  /** If the currently authenticated user liked the activity */
  isLiked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLiked", level })
    return this
  },
  /** If the activity is pinned to the top of the users activity feed */
  isPinned(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isPinned", level })
    return this
  },
  /** The time the activity was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The user who created the activity */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The written replies to the activity */
  replies(op?: { alias?: string; fn?: Fn<typeof ActivityReply> }) {
    operation.set({ alias: op?.alias, subField: "replies", level, hasSubField: true })
    if (op?.fn) op.fn(ActivityReply({ operation, level: level + 1 }))
    else ActivityReply({ operation, level: level + 1 }).id()
    return this
  },
  /** The users who liked the activity */
  likes(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "likes", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

export const ListActivity = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the activity */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The user id of the activity's creator */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The type of activity */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The number of activity replies */
  replyCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "replyCount", level })
    return this
  },
  /** The list item's textual status */
  status(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "status", level })
    return this
  },
  /** The list progress made */
  progress(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "progress", level })
    return this
  },
  /** If the activity is locked and can receive replies */
  isLocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLocked", level })
    return this
  },
  /** If the currently authenticated user is subscribed to the activity */
  isSubscribed(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isSubscribed", level })
    return this
  },
  /** The amount of likes the activity has */
  likeCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "likeCount", level })
    return this
  },
  /** If the currently authenticated user liked the activity */
  isLiked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLiked", level })
    return this
  },
  /** If the activity is pinned to the top of the users activity feed */
  isPinned(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isPinned", level })
    return this
  },
  /** The url for the activity page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** The time the activity was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The owner of the activity */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The associated media to the activity update */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "media", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  /** The written replies to the activity */
  replies(op?: { alias?: string; fn?: Fn<typeof ActivityReply> }) {
    operation.set({ alias: op?.alias, subField: "replies", level, hasSubField: true })
    if (op?.fn) op.fn(ActivityReply({ operation, level: level + 1 }))
    else ActivityReply({ operation, level: level + 1 }).id()
    return this
  },
  /** The users who liked the activity */
  likes(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "likes", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

export const MessageActivity = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the activity */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The user id of the activity's recipient */
  recipientId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "recipientId", level })
    return this
  },
  /** The user id of the activity's sender */
  messengerId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "messengerId", level })
    return this
  },
  /** The type of the activity */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The number of activity replies */
  replyCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "replyCount", level })
    return this
  },
  /** The message text (Markdown) */
  message(op?: { alias?: string; args?: MessageActivityMessageArgs }) {
    operation.set({ alias: op?.alias, subField: "message", level, variables: op?.args })
    return this
  },
  /** If the activity is locked and can receive replies */
  isLocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLocked", level })
    return this
  },
  /** If the currently authenticated user is subscribed to the activity */
  isSubscribed(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isSubscribed", level })
    return this
  },
  /** The amount of likes the activity has */
  likeCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "likeCount", level })
    return this
  },
  /** If the currently authenticated user liked the activity */
  isLiked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLiked", level })
    return this
  },
  /** If the message is private and only viewable to the sender and recipients */
  isPrivate(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isPrivate", level })
    return this
  },
  /** The url for the activity page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** The time the activity was created at */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The user who the activity message was sent to */
  recipient(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "recipient", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The user who sent the activity message */
  messenger(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "messenger", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The written replies to the activity */
  replies(op?: { alias?: string; fn?: Fn<typeof ActivityReply> }) {
    operation.set({ alias: op?.alias, subField: "replies", level, hasSubField: true })
    if (op?.fn) op.fn(ActivityReply({ operation, level: level + 1 }))
    else ActivityReply({ operation, level: level + 1 }).id()
    return this
  },
  /** The users who liked the activity */
  likes(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "likes", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})

export const ActivityUnion = ({ operation, level }: Fields<OperationParser>) => ({
  textActivity(op?: { alias?: string; fn?: Fn<typeof TextActivity> }) {
    operation.set({
      alias: op?.alias,
      subField: "TextActivity",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(TextActivity({ operation, level: level + 1 }))
    else TextActivity({ operation, level: level + 1 }).id()
    return this
  },
  listActivity(op?: { alias?: string; fn?: Fn<typeof ListActivity> }) {
    operation.set({
      alias: op?.alias,
      subField: "ListActivity",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(ListActivity({ operation, level: level + 1 }))
    else ListActivity({ operation, level: level + 1 }).id()
    return this
  },
  messageActivity(op?: { alias?: string; fn?: Fn<typeof MessageActivity> }) {
    operation.set({
      alias: op?.alias,
      subField: "MessageActivity",
      level,
      hasSubField: true,
      isUnion: true,
    })
    if (op?.fn) op.fn(MessageActivity({ operation, level: level + 1 }))
    else MessageActivity({ operation, level: level + 1 }).id()
    return this
  },
})
