import { Fields, OperationParser } from "../types/Anilist.ts"

export const Markdown = ({ operation, level }: Fields<OperationParser>) => ({
  /** The parsed markdown as html */
  html(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "html", level })
    return this
  },
})
