import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { Media } from "./Media.ts"
import { PageInfo } from "./Page.ts"

const MediaTrendEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op?: { alias?: string; fn?: Fn<typeof MediaTrend> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(MediaTrend({ operation, level: level + 1 }))
    else MediaTrend({ operation, level: level + 1 }).mediaId()
    return this
  },
})

export const MediaTrendConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof MediaTrendEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(MediaTrendEdge({ operation, level: level + 1 }))
    else MediaTrendEdge({ operation, level: level + 1 }).node()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof MediaTrend> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(MediaTrend({ operation, level: level + 1 }))
    else MediaTrend({ operation, level: level + 1 }).mediaId()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const MediaTrend = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the tag */
  mediaId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "mediaId", level })
    return this
  },
  /** The day the data was recorded (timestamp) */
  date(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "date", level })
    return this
  },
  /** The amount of media activity on the day */
  trending(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "trending", level })
    return this
  },
  /** A weighted average score of all the user's scores of the media */
  averageScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "averageScore", level })
    return this
  },
  /** The number of users with the media on their list */
  popularity(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "popularity", level })
    return this
  },
  /** The number of users with watching/reading the media */
  inProgress(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "inProgress", level })
    return this
  },
  /** If the media was being released at this time */
  releasing(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "releasing", level })
    return this
  },
  /** The episode number of the anime released on this day */
  episode(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "episode", level })
    return this
  },
  /** The related media */
  media(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
})
