import { Fields, OperationParser } from "../types/Anilist.ts"

export const ExternalLinkSourceCollection = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the external link */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The url of the external link or base url of link source */
  url(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "url", level })
    return this
  },
  /** The links website site name */
  site(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "site", level })
    return this
  },
  /** The links website site id */
  siteId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteId", level })
    return this
  },
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** Language the site content is in. See Staff language field for values. */
  language(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "language", level })
    return this
  },
  color(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "color", level })
    return this
  },
  /** The icon image url of the site. Not available for all links. Transparent PNG 64x64 */
  icon(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "icon", level })
    return this
  },
  notes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "notes", level })
    return this
  },
  isDisabled(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isDisabled", level })
    return this
  },
})
