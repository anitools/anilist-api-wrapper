import {
  MediaAiringScheduleArgs,
  MediaCharactersArgs,
  MediaDescriptionArgs,
  MediaEdgeRelationTypeArgs,
  MediaEdgeVoiceActorRolesArgs,
  MediaEdgeVoiceActorsArgs,
  MediaRecommendationsArgs,
  MediaReviewsArgs,
  MediaSourceArgs,
  MediaStaffArgs,
  MediaStatusArgs,
  MediaStudiosArgs,
  MediaTitleEnglishArgs,
  MediaTitleNativeArgs,
  MediaTitleRomajiArgs,
  MediaTrendsArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import type { AtLeastOne } from "../types/AtLeastOne.ts"
import { PageInfo } from "./Page.ts"
import { FuzzyDate, getFieldName } from "../anilist.ts"
import { MediaTag } from "./MediaTag.ts"
import { MediaList } from "./MediaList.ts"
import { AiringSchedule, AiringScheduleConnection } from "./AiringSchedule.ts"
import { Character, CharacterConnection } from "./Character.ts"
import { Staff, StaffConnection, StaffRoleType } from "./Staff.ts"
import { MediaTrendConnection } from "./MediaTrend.ts"
import { RecommendationConnection } from "./Recommendation.ts"
import { ReviewConnection } from "./Review.ts"
import { StudioConnection } from "./Studio.ts"

export const MediaListGroup = ({ operation, level }: Fields<OperationParser>) => ({
  /** Media list entries */
  entries(op: { alias?: string; fn: Fn<typeof MediaList> }) {
    operation.set({ alias: op.alias, subField: "entries", level, hasSubField: true })
    op.fn(MediaList({ operation, level: level + 1 }))
    return this
  },
  name(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "name", level })
    return this
  },
  isCustomList(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isCustomList", level })
    return this
  },
  isSplitCompletedList(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isSplitCompletedList", level })
    return this
  },
  status(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "status", level })
    return this
  },
})

const MediaTitle = ({ operation, level }: Fields<OperationParser>) => ({
  /** The romanization of the native language title */
  romaji(op?: { alias?: string; args?: MediaTitleRomajiArgs }) {
    operation.set({ alias: op?.alias, subField: "romaji", level, variables: op?.args })
    return this
  },
  /** The official english title */
  english(op?: { alias?: string; args?: MediaTitleEnglishArgs }) {
    operation.set({ alias: op?.alias, subField: "english", level, variables: op?.args })
    return this
  },
  /** Official title in it's native language */
  native(op?: { alias?: string; args?: MediaTitleNativeArgs }) {
    operation.set({ alias: op?.alias, subField: "native", level, variables: op?.args })
    return this
  },
  /** The currently authenticated users preferred title language. Default romaji for non-authenticated */
  userPreferred(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userPreferred", level })
    return this
  },
})

const MediaTrailer = ({ operation, level }: Fields<OperationParser>) => ({
  /** The trailer video id */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The site the video is hosted by (Currently either youtube or dailymotion) */
  site(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "site", level })
    return this
  },
  /** The url for the thumbnail image of the video */
  thumbnail(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "thumbnail", level })
    return this
  },
})

const MediaCoverImage = ({ operation, level }: Fields<OperationParser>) => ({
  /** The cover image url of the media at its largest size. If this size isn't available, large will be provided instead. */
  extraLarge(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "extraLarge", level })
    return this
  },
  /** The cover image url of the media at a large size */
  large(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "large", level })
    return this
  },
  /** The cover image url of the media at medium size */
  medium(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "medium", level })
    return this
  },
  /** Average #hex color of cover image */
  color(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "color", level })
    return this
  },
})

const MediaExternalLink = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the external link */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The url of the external link or base url of link source */
  url(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "url", level })
    return this
  },
  /** The links website site name */
  site(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "site", level })
    return this
  },
  /** The links website site id */
  siteId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteId", level })
    return this
  },
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** Language the site content is in. See Staff language field for values. */
  language(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "language", level })
    return this
  },
  color(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "color", level })
    return this
  },
  /** The icon image url of the site. Not available for all links. Transparent PNG 64x64 */
  icon(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "icon", level })
    return this
  },
  notes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "notes", level })
    return this
  },
  isDisabled(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isDisabled", level })
    return this
  },
})

const MediaStreamingEpisode = ({ operation, level }: Fields<OperationParser>) => ({
  /** Title of the episode */
  title(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "title", level })
    return this
  },
  /** Url of episode image thumbnail */
  thumbnail(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "thumbnail", level })
    return this
  },
  /** The url of the episode */
  url(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "url", level })
    return this
  },
  /** The site location of the streaming episodes */
  site(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "site", level })
    return this
  },
})

const MediaRank = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the rank */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The numerical rank of the media */
  rank(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "rank", level })
    return this
  },
  /** The type of ranking */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The format the media is ranked within */
  format(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "format", level })
    return this
  },
  /** The year the media is ranked within */
  year(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "year", level })
    return this
  },
  /** The season the media is ranked within */
  season(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "season", level })
    return this
  },
  /** If the ranking is based on all time instead of a season/year */
  allTime(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "allTime", level })
    return this
  },
  /** String that gives context to the ranking type and time span */
  context(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "context", level })
    return this
  },
})

const MediaStats = ({ operation, level }: Fields<OperationParser>) => ({
  scoreDistribution(op: { alias?: string; fn: Fn<typeof ScoreDistribution> }) {
    operation.set({ alias: op.alias, subField: "scoreDistribution", level, hasSubField: true })
    op.fn(ScoreDistribution({ operation, level: level + 1 }))
    return this
  },
  statusDistribution(op: { alias?: string; fn: Fn<typeof StatusDistribution> }) {
    operation.set({ alias: op.alias, subField: "statusDistribution", level, hasSubField: true })
    op.fn(StatusDistribution({ operation, level: level + 1 }))
    return this
  },
})

const StatusDistribution = ({ operation, level }: Fields<OperationParser>) => ({
  status(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "status", level })
    return this
  },
  amount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "amount", level })
    return this
  },
})

const ScoreDistribution = ({ operation, level }: Fields<OperationParser>) => ({
  score(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "score", level })
    return this
  },
  amount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "amount", level })
    return this
  },
})

const MediaEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  /** The id of the connection */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The type of relation to the parent model */
  relationType(op?: { alias?: string; args?: AtLeastOne<MediaEdgeRelationTypeArgs> }) {
    operation.set({ alias: op?.alias, subField: "relationType", level, variables: op?.args })
    return this
  },
  /** If the studio is the main animation studio of the media (For Studio->MediaConnection field only) */
  isMainStudio(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isMainStudio", level })
    return this
  },
  /** The characters in the media voiced by the parent actor */
  characters(op: { alias?: string; fn: Fn<typeof Character> }) {
    operation.set({ alias: op.alias, subField: "characters", level, hasSubField: true })
    op.fn(Character({ operation, level: level + 1 }))
    return this
  },
  /** The characters role in the media */
  characterRole(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "characterRole", level })
    return this
  },
  /** Media specific character name */
  characterName(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "characterName", level })
    return this
  },
  /** Notes regarding the VA's role for the character */
  roleNotes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "roleNotes", level })
    return this
  },
  /** Used for grouping roles where multiple dubs exist for the same language. Either dubbing company name or language variant. */
  dubGroup(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "dubGroup", level })
    return this
  },
  /** The role of the staff member in the production of the media */
  staffRole(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "staffRole", level })
    return this
  },
  /** The voice actors of the character */
  voiceActors(
    op?: { alias?: string; args?: AtLeastOne<MediaEdgeVoiceActorsArgs>; fn?: Fn<typeof Staff> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "voiceActors",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  /** The voice actors of the character with role date */
  voiceActorRoles(op: {
    args?: AtLeastOne<MediaEdgeVoiceActorRolesArgs>
    fn: Fn<typeof StaffRoleType>
  }) {
    operation.set({
      subField: "voiceActorRoles",
      level,
      variables: op.args,
      hasSubField: true,
    })
    op.fn(StaffRoleType({ operation, level: level + 1 }))
    return this
  },
  /** The order the media should be displayed from the users favourites */
  favouriteOrder(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favouriteOrder", level })
    return this
  },
})

export const MediaConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof MediaEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(MediaEdge({ operation, level: level + 1 }))
    else MediaEdge({ operation, level: level + 1 }).id()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof Media> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

export const Media = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the media */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The mal id of the media */
  idMal(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "idMal", level })
    return this
  },
  /** The type of the media; anime or manga */
  type(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "type", level })
    return this
  },
  /** The format the media was released in */
  format(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "format", level })
    return this
  },
  /** The season the media was initially released in */
  season(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "season", level })
    return this
  },
  /** The season year the media was initially released in */
  seasonYear(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "seasonYear", level })
    return this
  },
  /** The year & season the media was initially released in */
  seasonInt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "seasonInt", level })
    return this
  },
  /** The amount of episodes the anime has when complete */
  episodes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "episodes", level })
    return this
  },
  /** The general length of each anime episode in minutes */
  duration(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "duration", level })
    return this
  },
  /** The amount of chapters the manga has when complete */
  chapters(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "chapters", level })
    return this
  },
  /** The amount of volumes the manga has when complete */
  volumes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "volumes", level })
    return this
  },
  /** Where the media was created. (ISO 3166-1 alpha-2) */
  countryOfOrigin(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "countryOfOrigin", level })
    return this
  },
  /** If the media is officially licensed or a self-published doujin release */
  isLicensed(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLicensed", level })
    return this
  },
  /** Official Twitter hashtags for the media */
  hashtag(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "hashtag", level })
    return this
  },
  /** When the media's data was last updated */
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedAt", level })
    return this
  },
  /** The banner image of the media */
  bannerImage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "bannerImage", level })
    return this
  },
  /** The genres of the media */
  genres(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "genres", level })
    return this
  },
  /** Alternative titles of the media */
  synonyms(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "synonyms", level })
    return this
  },
  /** A weighted average score of all the user's scores of the media */
  averageScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "averageScore", level })
    return this
  },
  /** Mean score of all the user's scores of the media */
  meanScore(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "meanScore", level })
    return this
  },
  /** The number of users with the media on their list */
  popularity(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "popularity", level })
    return this
  },
  /** Locked media may not be added to lists our favorited. This may be due to the entry pending for deletion or other reasons. */
  isLocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLocked", level })
    return this
  },
  /** The amount of related activity in the past hour */
  trending(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "trending", level })
    return this
  },
  /** The amount of user's who have favourited the media */
  favourites(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favourites", level })
    return this
  },
  /** If the media is marked as favourite by the current authenticated user */
  isFavourite(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavourite", level })
    return this
  },
  /** If the media is blocked from being added to favourites */
  isFavouriteBlocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavouriteBlocked", level })
    return this
  },
  /** If the media is intended only for 18+ adult audiences */
  isAdult(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isAdult", level })
    return this
  },
  /** The media's next episode airing schedule */
  nextAiringEpisode(op?: { alias?: string; fn?: Fn<typeof AiringSchedule> }) {
    operation.set({ alias: op?.alias, subField: "nextAiringEpisode", level, hasSubField: true })
    if (op?.fn) op.fn(AiringSchedule({ operation, level: level + 1 }))
    else AiringSchedule({ operation, level: level + 1 }).id()
    return this
  },
  /** The url for the media page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** If the media should have forum thread automatically created for it on airing episode release */
  autoCreateForumThread(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "autoCreateForumThread", level })
    return this
  },
  /** If the media is blocked from being recommended to/from */
  isRecommendationBlocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isRecommendationBlocked", level })
    return this
  },
  /** If the media is blocked from being reviewed */
  isReviewBlocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isReviewBlocked", level })
    return this
  },
  /** Notes for site moderators */
  modNotes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "modNotes", level })
    return this
  },
  /** The current releasing status of the media */
  status(op?: { alias?: string; args?: AtLeastOne<MediaStatusArgs> }) {
    operation.set({ alias: op?.alias, subField: "status", level, variables: op?.args })
    return this
  },
  /** Short description of the media's story and characters */
  description(op?: { alias?: string; args?: AtLeastOne<MediaDescriptionArgs> }) {
    operation.set({ alias: op?.alias, subField: "description", level, variables: op?.args })
    return this
  },
  /** Source type the media was adapted from. */
  source(op?: { alias?: string; args?: AtLeastOne<MediaSourceArgs> }) {
    operation.set({ alias: op?.alias, subField: "source", level, variables: op?.args })
    return this
  },
  /** The staff who produced the media */
  staff(
    op: { alias?: string; args?: AtLeastOne<MediaStaffArgs>; fn: Fn<typeof StaffConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "staff",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(StaffConnection({ operation, level: level + 1 }))
    return this
  },
  /** User recommendations for similar media */
  recommendations(
    op: {
      alias?: string
      args?: AtLeastOne<MediaRecommendationsArgs>
      fn: Fn<typeof RecommendationConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "recommendations",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(RecommendationConnection({ operation, level: level + 1 }))
    return this
  },
  /** The characters in the media */
  characters(op: {
    args?: AtLeastOne<MediaCharactersArgs>
    fn: Fn<typeof CharacterConnection>
  }) {
    operation.set({
      subField: "characters",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(CharacterConnection({ operation, level: level + 1 }))
    return this
  },
  /** The companies who produced the media */
  studios(op: {
    args?: AtLeastOne<MediaStudiosArgs>
    fn: Fn<typeof StudioConnection>
  }) {
    operation.set({ subField: "studios", level, variables: op?.args, hasSubField: true })
    op.fn(StudioConnection({ operation, level: level + 1 }))
    return this
  },
  /** The media's entire airing schedule */
  airingSchedule(op: {
    args?: AtLeastOne<MediaAiringScheduleArgs>
    fn: Fn<typeof AiringScheduleConnection>
  }) {
    operation.set({
      subField: "airingSchedule",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(AiringScheduleConnection({ operation, level: level + 1 }))
    return this
  },
  /** The media's daily trend stats */
  trends(
    op: { alias?: string; args?: AtLeastOne<MediaTrendsArgs>; fn: Fn<typeof MediaTrendConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "trends",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(MediaTrendConnection({ operation, level: level + 1 }))
    return this
  },
  /** User reviews of the media */
  reviews(
    op: { alias?: string; args?: AtLeastOne<MediaReviewsArgs>; fn: Fn<typeof ReviewConnection> },
  ) {
    operation.set({
      alias: op.alias,
      subField: "reviews",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(ReviewConnection({ operation, level: level + 1 }))
    return this
  },
  /** The official titles of the media in various languages */
  title(op: { alias?: string; fn: Fn<typeof MediaTitle> }) {
    operation.set({ alias: op.alias, subField: "title", level, hasSubField: true })
    op.fn(MediaTitle({ operation, level: level + 1 }))
    return this
  },
  /** The first official release date of the media */
  startDate(op?: { alias?: string; fn?: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op?.alias, subField: "startDate", level, hasSubField: true })
    if (op?.fn) op.fn(FuzzyDate({ operation, level: level + 1 }))
    else {
      FuzzyDate({ operation, level: level + 1 })
        .year()
        .month()
        .day()
    }
    return this
  },
  /** The last official release date of the media */
  endDate(op?: { alias?: string; fn?: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op?.alias, subField: "endDate", level, hasSubField: true })
    if (op?.fn) op.fn(FuzzyDate({ operation, level: level + 1 }))
    else {
      FuzzyDate({ operation, level: level + 1 })
        .year()
        .month()
        .day()
    }
    return this
  },
  /** Media trailer or advertisement */
  trailer(op: { alias?: string; fn: Fn<typeof MediaTrailer> }) {
    operation.set({ alias: op.alias, subField: "trailer", level, hasSubField: true })
    op.fn(MediaTrailer({ operation, level: level + 1 }))
    return this
  },
  /** The cover images of the media */
  coverImage(op?: { alias?: string; fn?: Fn<typeof MediaCoverImage> }) {
    operation.set({ alias: op?.alias, subField: "coverImage", level, hasSubField: true })
    if (op?.fn) op.fn(MediaCoverImage({ operation, level: level + 1 }))
    else MediaCoverImage({ operation, level: level + 1 }).medium()
    return this
  },
  /** List of tags that describes elements and themes of the media */
  tags(op: { alias?: string; fn: Fn<typeof MediaTag> }) {
    operation.set({ alias: op.alias, subField: "tags", level, hasSubField: true })
    op.fn(MediaTag({ operation, level: level + 1 }))
    return this
  },
  /** Other media in the same or connecting franchise */
  relations(op: { alias?: string; fn: Fn<typeof MediaConnection> }) {
    operation.set({ alias: op.alias, subField: "relations", level, hasSubField: true })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** External links to another site related to the media */
  externalLinks(op: { alias?: string; fn: Fn<typeof MediaExternalLink> }) {
    operation.set({ alias: op.alias, subField: "externalLinks", level, hasSubField: true })
    op.fn(MediaExternalLink({ operation, level: level + 1 }))
    return this
  },
  /** Data and links to legal streaming episodes on external sites */
  streamingEpisodes(op: { alias?: string; fn: Fn<typeof MediaStreamingEpisode> }) {
    operation.set({ alias: op.alias, subField: "streamingEpisodes", level, hasSubField: true })
    op.fn(MediaStreamingEpisode({ operation, level: level + 1 }))
    return this
  },
  /** The ranking of the media in a particular time span and format compared to other media */
  rankings(op: { alias?: string; fn: Fn<typeof MediaRank> }) {
    operation.set({ alias: op.alias, subField: "rankings", level, hasSubField: true })
    op.fn(MediaRank({ operation, level: level + 1 }))
    return this
  },
  /** The authenticated user's media list entry for the media */
  mediaListEntry(op?: { alias?: string; fn?: Fn<typeof MediaList> }) {
    operation.set({ alias: op?.alias, subField: "mediaListEntry", level, hasSubField: true })
    if (op?.fn) op.fn(MediaList({ operation, level: level + 1 }))
    else MediaList({ operation, level: level + 1 }).id()
    return this
  },
  stats(op: { alias?: string; fn: Fn<typeof MediaStats> }) {
    operation.set({ alias: op.alias, subField: "stats", level, hasSubField: true })
    op.fn(MediaStats({ operation, level: level + 1 }))
    return this
  },
})
