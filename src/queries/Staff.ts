import {
  StaffCharacterMediaArgs,
  StaffCharactersArgs,
  StaffStaffMediaArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { FuzzyDate } from "../anilist.ts"
import { PageInfo } from "./Page.ts"
import { User } from "./User.ts"
import { CharacterConnection } from "./Character.ts"
import { MediaConnection } from "./Media.ts"
import { AtLeastOne } from "../types/AtLeastOne.ts"

export const StaffRoleType = ({ operation, level }: Fields<OperationParser>) => ({
  /** The voice actors of the character */
  voiceActor(op: { alias?: string; fn: Fn<typeof Staff> }) {
    operation.set({ alias: op.alias, subField: "voiceActor", level, hasSubField: true })
    op.fn(Staff({ operation, level: level + 1 }))
    return this
  },
  /** Notes regarding the VA's role for the character */
  roleNotes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "roleNotes", level })
    return this
  },
  /** Used for grouping roles where multiple dubs exist for the same language. Either dubbing company name or language variant. */
  dubGroup(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "dubGroup", level })
    return this
  },
})

const StaffEdge = ({ operation, level }: Fields<OperationParser>) => ({
  node(op?: { alias?: string; fn?: Fn<typeof Staff> }) {
    operation.set({ alias: op?.alias, subField: "node", level, hasSubField: true })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  /** The id of the connection */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The role of the staff member in the production of the media */
  role(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "role", level })
    return this
  },
  /** The order the staff should be displayed from the users favourites */
  favouriteOrder(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favouriteOrder", level })
    return this
  },
})

export const StaffConnection = ({ operation, level }: Fields<OperationParser>) => ({
  edges(op?: { alias?: string; fn?: Fn<typeof StaffEdge> }) {
    operation.set({ alias: op?.alias, subField: "edges", level, hasSubField: true })
    if (op?.fn) op.fn(StaffEdge({ operation, level: level + 1 }))
    else StaffEdge({ operation, level: level + 1 }).node()
    return this
  },
  nodes(op?: { alias?: string; fn?: Fn<typeof Staff> }) {
    operation.set({ alias: op?.alias, subField: "nodes", level, hasSubField: true })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
})

const StaffName = ({ operation, level }: Fields<OperationParser>) => ({
  /** The person's given name */
  first(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "first", level })
    return this
  },
  /** The person's middle name */
  middle(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "middle", level })
    return this
  },
  /** The person's surname */
  last(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "last", level })
    return this
  },
  /** The person's first and last name */
  full(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "full", level })
    return this
  },
  /** The person's full name in their native language */
  native(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "native", level })
    return this
  },
  /** Other names the staff member might be referred to as (pen names) */
  alternative(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "alternative", level })
    return this
  },
  /** The currently authenticated users preferred name language. Default romaji for non-authenticated */
  userPreferred(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userPreferred", level })
    return this
  },
})

const StaffImage = ({ operation, level }: Fields<OperationParser>) => ({
  /** The person's image of media at its largest size */
  large(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "large", level })
    return this
  },
  /** The person's image of media at medium size */
  medium(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "medium", level })
    return this
  },
})

export const Staff = ({ operation, level }: Fields<OperationParser>) => ({
  /**   The id of the staff member */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The names of the staff member */
  name(op?: { alias?: string; fn?: Fn<typeof StaffName> }) {
    operation.set({ alias: op?.alias, subField: "name", level, hasSubField: true })
    if (op?.fn) op.fn(StaffName({ operation, level: level + 1 }))
    else StaffName({ operation, level: level + 1 }).userPreferred()
    return this
  },
  /** The primary language of the staff member. Current values: Japanese, English, Korean, Italian, Spanish, Portuguese, French, German, Hebrew, Hungarian, Chinese, Arabic, Filipino, Catalan, Finnish, Turkish, Dutch, Swedish, Thai, Tagalog, Malaysian, Indonesian, Vietnamese, Nepali, Hindi, Urdu */
  languageV2(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "languageV2", level })
    return this
  },
  /** The staff images */
  image(op?: { alias?: string; fn?: Fn<typeof StaffImage> }) {
    operation.set({ alias: op?.alias, subField: "image", level, hasSubField: true })
    if (op?.fn) op.fn(StaffImage({ operation, level: level + 1 }))
    else StaffImage({ operation, level: level + 1 }).medium()
    return this
  },
  /** A general description of the staff member */
  description(op?: { alias?: string; args?: { asHtml: boolean } }) {
    operation.set({ alias: op?.alias, subField: "description", level, variables: op?.args })
    return this
  },
  /** The person's primary occupations */
  primaryOccupations(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "primaryOccupations", level })
    return this
  },
  /** The staff's gender. Usually Male, Female, or Non-binary but can be any string. */
  gender(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "gender", level })
    return this
  },
  dateOfBirth(op?: { alias?: string; fn?: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op?.alias, subField: "dateOfBirth", level, hasSubField: true })
    if (op?.fn) op.fn(FuzzyDate({ operation, level: level + 1 }))
    else {
      FuzzyDate({ operation, level: level + 1 })
        .year()
        .month()
        .day()
    }
    return this
  },
  dateOfDeath(op?: { alias?: string; fn?: Fn<typeof FuzzyDate> }) {
    operation.set({ alias: op?.alias, subField: "dateOfDeath", level, hasSubField: true })
    if (op?.fn) op.fn(FuzzyDate({ operation, level: level + 1 }))
    else {
      FuzzyDate({ operation, level: level + 1 })
        .year()
        .month()
        .day()
    }
    return this
  },
  /** The person's age in years */
  age(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "age", level })
    return this
  },
  /** [startYear, endYear] (If the 2nd value is not present staff is still active) */
  yearsActive(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "yearsActive", level })
    return this
  },
  /** The persons birthplace or hometown */
  homeTown(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "homeTown", level })
    return this
  },
  /** The persons blood type */
  bloodType(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "bloodType", level })
    return this
  },
  /** If the staff member is marked as favourite by the currently authenticated user */
  isFavourite(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavourite", level })
    return this
  },
  /** If the staff member is blocked from being added to favourites */
  isFavouriteBlocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isFavouriteBlocked", level })
    return this
  },
  /** The url for the staff page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** Media where the staff member has a production role */
  staffMedia(
    op: {
      alias?: string
      args?: AtLeastOne<StaffStaffMediaArgs> | undefined
      fn: Fn<typeof MediaConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "staffMedia",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** Characters voiced by the actor */
  characters(
    op: {
      alias?: string
      args?: AtLeastOne<StaffCharactersArgs> | undefined
      fn: Fn<typeof CharacterConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "characters",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(CharacterConnection({ operation, level: level + 1 }))
    return this
  },
  /** Media the actor voiced characters in. (Same data as characters with media as node instead of characters) */
  characterMedia(
    op: {
      alias?: string
      args?: AtLeastOne<StaffCharacterMediaArgs>
      fn: Fn<typeof MediaConnection>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "characterMedia",
      level,
      variables: op?.args,
      hasSubField: true,
    })
    op.fn(MediaConnection({ operation, level: level + 1 }))
    return this
  },
  /** Staff member that the submission is referencing */
  staff(op?: { alias?: string; fn?: Fn<typeof Staff> }) {
    operation.set({ alias: op?.alias, subField: "staff", level, hasSubField: true })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  /** Submitter for the submission */
  submitter(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "submitter", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** Status of the submission */
  submissionStatus(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "submissionStatus", level })
    return this
  },
  /** Inner details of submission status */
  submissionNotes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "submissionNotes", level })
    return this
  },
  /** The amount of user's who have favourited the staff member */
  favourites(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "favourites", level })
    return this
  },
  /** Notes for site moderators */
  modNotes(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "modNotes", level })
    return this
  },
})
