import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { User } from "./User.ts"

export const AniChartUser = ({ operation, level }: Fields<OperationParser>) => ({
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  settings(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "settings", level })
    return this
  },
  highlights(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "highlights", level })
    return this
  },
})
