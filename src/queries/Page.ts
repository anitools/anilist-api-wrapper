import { Fields, Fn, OperationParser } from "../types/Anilist.ts"
import { AtLeastOne } from "../types/AtLeastOne.ts"
import {
  PageActivitiesArgs,
  PageActivityRepliesArgs,
  PageAiringSchedulesArgs,
  PageCharactersArgs,
  PageFollowersArgs,
  PageFollowingArgs,
  //   PageInfo,
  PageLikesArgs,
  PageMediaArgs,
  PageMediaListArgs,
  PageMediaTrendsArgs,
  PageNotificationsArgs,
  PageRecommendationsArgs,
  PageReviewsArgs,
  PageStaffArgs,
  PageStudiosArgs,
  PageThreadCommentsArgs,
  PageThreadsArgs,
  PageUsersArgs,
} from "../../graphql-codegen/generated/graphql.ts"
import { ActivityReply } from "./ActivityReply.ts"
import { ActivityUnion } from "./ActivityUnion.ts"
import { AiringSchedule } from "./AiringSchedule.ts"
import { Character } from "./Character.ts"
import { Media } from "./Media.ts"
import { MediaList } from "./MediaList.ts"
import { MediaTrend } from "./MediaTrend.ts"
import { NotificationUnion } from "./NotificationUnion.ts"
import { Recommendation } from "./Recommendation.ts"
import { Review } from "./Review.ts"
import { Staff } from "./Staff.ts"
import { Studio } from "./Studio.ts"
import { Thread } from "./Thread.ts"
import { ThreadComment } from "./ThreadComment.ts"
import { User } from "./User.ts"

export const PageInfo = ({ operation, level }: Fields<OperationParser>) => ({
  /** The total number of items. Note: This value is not guaranteed to be accurate, do not rely on this for logic */
  total(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "total", level })
    return this
  },
  /** The count on a page */
  perPage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "perPage", level })
    return this
  },
  /** The current page */
  currentPage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "currentPage", level })
    return this
  },
  /** The last page */
  lastPage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "lastPage", level })
    return this
  },
  /** If there is another page */
  hasNextPage(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "hasNextPage", level })
    return this
  },
})

export const Page = ({ operation, level }: Fields<OperationParser>) => ({
  /** The pagination information */
  pageInfo(op: { alias?: string; fn: Fn<typeof PageInfo> }) {
    operation.set({ alias: op.alias, subField: "pageInfo", level, hasSubField: true })
    op.fn(PageInfo({ operation, level: level + 1 }))
    return this
  },
  users(op?: { alias?: string; args?: AtLeastOne<PageUsersArgs>; fn?: Fn<typeof User> }) {
    operation.set({
      alias: op?.alias,
      subField: "users",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  media(op?: { alias?: string; args?: AtLeastOne<PageMediaArgs>; fn?: Fn<typeof Media> }) {
    operation.set({
      alias: op?.alias,
      subField: "media",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Media({ operation, level: level + 1 }))
    else Media({ operation, level: level + 1 }).id()
    return this
  },
  characters(
    op?: { alias?: string; args?: AtLeastOne<PageCharactersArgs>; fn?: Fn<typeof Character> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "characters",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Character({ operation, level: level + 1 }))
    else Character({ operation, level: level + 1 }).id()
    return this
  },
  staff(op?: { alias?: string; args?: AtLeastOne<PageStaffArgs>; fn?: Fn<typeof Staff> }) {
    operation.set({
      alias: op?.alias,
      subField: "staff",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Staff({ operation, level: level + 1 }))
    else Staff({ operation, level: level + 1 }).id()
    return this
  },
  studios(op?: { alias?: string; args?: AtLeastOne<PageStudiosArgs>; fn?: Fn<typeof Studio> }) {
    operation.set({
      alias: op?.alias,
      subField: "studios",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Studio({ operation, level: level + 1 }))
    else Studio({ operation, level: level + 1 }).id()
    return this
  },
  mediaList(
    op?: { alias?: string; args?: AtLeastOne<PageMediaListArgs>; fn?: Fn<typeof MediaList> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "mediaList",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(MediaList({ operation, level: level + 1 }))
    else MediaList({ operation, level: level + 1 }).id()
    return this
  },
  airingSchedules(
    op?: {
      alias?: string
      args?: AtLeastOne<PageAiringSchedulesArgs>
      fn?: Fn<typeof AiringSchedule>
    },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "airingSchedules",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(AiringSchedule({ operation, level: level + 1 }))
    else AiringSchedule({ operation, level: level + 1 }).id()
    return this
  },
  mediaTrends(
    op?: { alias?: string; args?: AtLeastOne<PageMediaTrendsArgs>; fn?: Fn<typeof MediaTrend> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "mediaTrends",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(MediaTrend({ operation, level: level + 1 }))
    else MediaTrend({ operation, level: level + 1 }).mediaId()
    return this
  },
  notifications(
    op: {
      alias?: string
      args?: AtLeastOne<PageNotificationsArgs>
      fn: Fn<typeof NotificationUnion>
    },
  ) {
    operation.set({
      alias: op.alias,
      subField: "notifications",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    op.fn(NotificationUnion({ operation, level: level + 1 }))
    return this
  },
  followers(
    op?: { alias?: string; args?: AtLeastOne<PageFollowersArgs>; fn?: Fn<typeof User> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "followers",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  following(
    op?: { alias?: string; args?: AtLeastOne<PageFollowingArgs>; fn?: Fn<typeof User> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "following",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  activities(
    op?: { alias?: string; args?: AtLeastOne<PageActivitiesArgs>; fn?: Fn<typeof ActivityUnion> },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "activities",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(ActivityUnion({ operation, level: level + 1 }))
    else {
      ActivityUnion({ operation, level: level + 1 })
        .listActivity()
        .messageActivity()
        .textActivity()
    }
    return this
  },
  activityReplies(
    op?: {
      alias?: string
      args?: AtLeastOne<PageActivityRepliesArgs>
      fn?: Fn<typeof ActivityReply>
    },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "activityReplies",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(ActivityReply({ operation, level: level + 1 }))
    else ActivityReply({ operation, level: level + 1 }).id()
    return this
  },
  threads(op?: { alias?: string; args?: AtLeastOne<PageThreadsArgs>; fn?: Fn<typeof Thread> }) {
    operation.set({
      alias: op?.alias,
      subField: "threads",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Thread({ operation, level: level + 1 }))
    else Thread({ operation, level: level + 1 }).id()
    return this
  },
  threadComments(
    op?: {
      alias?: string
      args?: AtLeastOne<PageThreadCommentsArgs>
      fn?: Fn<typeof ThreadComment>
    },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "threadComments",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(ThreadComment({ operation, level: level + 1 }))
    else ThreadComment({ operation, level: level + 1 }).id()
    return this
  },
  reviews(op?: { alias?: string; args?: AtLeastOne<PageReviewsArgs>; fn?: Fn<typeof Review> }) {
    operation.set({
      alias: op?.alias,
      subField: "reviews",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Review({ operation, level: level + 1 }))
    else Review({ operation, level: level + 1 }).id()
    return this
  },
  recommendations(
    op?: {
      alias?: string
      args?: AtLeastOne<PageRecommendationsArgs>
      fn?: Fn<typeof Recommendation>
    },
  ) {
    operation.set({
      alias: op?.alias,
      subField: "recommendations",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(Recommendation({ operation, level: level + 1 }))
    else Recommendation({ operation, level: level + 1 }).id()
    return this
  },
  likes(op?: { alias?: string; args?: AtLeastOne<PageLikesArgs>; fn?: Fn<typeof User> }) {
    operation.set({
      alias: op?.alias,
      subField: "likes",
      level,
      hasSubField: true,
      variables: op?.args,
    })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
})
