import { User } from "./User.ts"
import { Thread } from "./Thread.ts"
import { Fields, Fn, OperationParser } from "../types/Anilist.ts"

export const ThreadComment = ({ operation, level }: Fields<OperationParser>) => ({
  /** The id of the comment */
  id(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "id", level })
    return this
  },
  /** The user id of the comment's owner */
  userId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "userId", level })
    return this
  },
  /** The id of thread the comment belongs to */
  threadId(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "threadId", level })
    return this
  },
  /** The text content of the comment (Markdown) */
  comment(op?: { alias?: string; args?: { asHtml: boolean } }) {
    operation.set({ alias: op?.alias, subField: "comment", level, variables: op?.args })
    return this
  },
  /** The amount of likes the comment has */
  likeCount(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "likeCount", level })
    return this
  },
  /** If the currently authenticated user liked the comment */
  isLiked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLiked", level })
    return this
  },
  /** The url for the comment page on the AniList website */
  siteUrl(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "siteUrl", level })
    return this
  },
  /** The time of the comments creation */
  createdAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "createdAt", level })
    return this
  },
  /** The time of the comments last update */
  updatedAt(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "updatedAt", level })
    return this
  },
  /** The thread the comment belongs to */
  thread(op?: { alias?: string; fn?: Fn<typeof Thread> }) {
    operation.set({ alias: op?.alias, subField: "thread", level, hasSubField: true })
    if (op?.fn) op.fn(Thread({ operation, level: level + 1 }))
    else Thread({ operation, level: level + 1 }).id()
    return this
  },
  /** The user who created the comment */
  user(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "user", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The users who liked the comment */
  likes(op?: { alias?: string; fn?: Fn<typeof User> }) {
    operation.set({ alias: op?.alias, subField: "likes", level, hasSubField: true })
    if (op?.fn) op.fn(User({ operation, level: level + 1 }))
    else User({ operation, level: level + 1 }).id()
    return this
  },
  /** The comment's child reply comments */
  childComments(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "childComments", level })
    return this
  },
  /** If the comment tree is locked and may not receive replies or edits */
  isLocked(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "isLocked", level })
    return this
  },
})
