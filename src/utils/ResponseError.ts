export type ResponseError = {
  errors: Array<{
    message: string
    status: number
    locations: Array<Record<"line" | "column", number>>
  }>
  data: Record<string, null>
}

export const ResponseError = class extends Error {
  errors: ResponseError["errors"]
  data: ResponseError["data"]
  constructor(message: ResponseError) {
    super()

    this.name = "ResponseError"
    this.errors = message.errors
    this.data = message.data
  }
}
