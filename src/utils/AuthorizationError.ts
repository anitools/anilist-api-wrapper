export type AuthorizationError = { error: string; message: string; hint?: string }

export const AuthorizationError = class extends Error {
  hint?: string
  constructor(message: AuthorizationError) {
    super()

    this.name = "AuthorizationError"
    this.message = message.error
    this.cause = message.message
    this.hint = message.hint
  }
}
