const Case = {
  up: "toUpperCase",
  down: "toLowerCase",
} as const

export const toCase = <T extends string>(type: keyof typeof Case, str: T) =>
  str[0][Case[type]]() + str.slice(1) as Capitalize<T>
