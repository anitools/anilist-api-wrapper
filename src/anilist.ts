import {
  MutationDeleteActivityArgs,
  MutationDeleteActivityReplyArgs,
  MutationDeleteCustomListArgs,
  MutationDeleteMediaListEntryArgs,
  MutationDeleteReviewArgs,
  MutationDeleteThreadArgs,
  MutationDeleteThreadCommentArgs,
  MutationRateReviewArgs,
  MutationSaveActivityReplyArgs,
  MutationSaveListActivityArgs,
  MutationSaveMediaListEntryArgs,
  MutationSaveMessageActivityArgs,
  MutationSaveRecommendationArgs,
  MutationSaveReviewArgs,
  MutationSaveTextActivityArgs,
  MutationSaveThreadArgs,
  MutationSaveThreadCommentArgs,
  MutationToggleActivityPinArgs,
  MutationToggleActivitySubscriptionArgs,
  MutationToggleFavouriteArgs,
  MutationToggleFollowArgs,
  MutationToggleLikeArgs,
  MutationToggleLikeV2Args,
  MutationToggleThreadSubscriptionArgs,
  MutationUpdateFavouriteOrderArgs,
  MutationUpdateMediaListEntriesArgs,
  MutationUpdateUserArgs,
  QueryActivityArgs,
  QueryActivityReplyArgs,
  QueryAiringScheduleArgs,
  QueryCharacterArgs,
  QueryExternalLinkSourceCollectionArgs,
  QueryFollowerArgs,
  QueryFollowingArgs,
  QueryLikeArgs,
  QueryMarkdownArgs,
  QueryMediaArgs,
  QueryMediaListArgs,
  QueryMediaListCollectionArgs,
  QueryMediaTagCollectionArgs,
  QueryMediaTrendArgs,
  QueryNotificationArgs,
  QueryPageArgs,
  QueryRecommendationArgs,
  QueryReviewArgs,
  QueryStaffArgs,
  QueryStudioArgs,
  QueryThreadArgs,
  QueryThreadCommentArgs,
  QueryUserArgs,
} from "../graphql-codegen/generated/graphql.ts"
import { Fetch, Fields, Fn, OperationParser } from "./types/Anilist.ts"
import type { AtLeastOne } from "./types/AtLeastOne.ts"
import { Media } from "./queries/Media.ts"
import { MediaTrend } from "./queries/MediaTrend.ts"
import { AiringSchedule } from "./queries/AiringSchedule.ts"
import { Character } from "./queries/Character.ts"
import { Staff } from "./queries/Staff.ts"
import { MediaList } from "./queries/MediaList.ts"
import { MediaListCollection } from "./queries/MediaListCollection.ts"
import { MediaTag } from "./queries/MediaTag.ts"
import { User } from "./queries/User.ts"
import { Studio } from "./queries/Studio.ts"
import { Review } from "./queries/Review.ts"
import { ActivityReply } from "./queries/ActivityReply.ts"
import { Thread } from "./queries/Thread.ts"
import { ThreadComment } from "./queries/ThreadComment.ts"
import { Recommendation } from "./queries/Recommendation.ts"
import { AniChartUser } from "./queries/AniChartUser.ts"
import { Markdown } from "./queries/Markdown.ts"
import { SiteStatistics } from "./queries/SiteStatistics.ts"
import { ExternalLinkSourceCollection } from "./queries/ExternalLinkSourceCollection.ts"
import { AuthorizationError } from "./utils/AuthorizationError.ts"
import { ResponseError } from "./utils/ResponseError.ts"
import { Favourites } from "./queries/Favourites.ts"
import {
  ActivityUnion,
  ListActivity,
  MessageActivity,
  TextActivity,
} from "./queries/ActivityUnion.ts"
import { LikeableUnion } from "./queries/LikeableUnion.ts"
import { NotificationUnion } from "./queries/NotificationUnion.ts"
import { Page } from "./queries/Page.ts"
import { toCase } from "./utils/toCase.ts"

const rewriteVarValues = (
  variable: PropertyKey | unknown | (PropertyKey | unknown)[],
) => {
  switch (typeof variable) {
    case "object":
      return `[${variable}]`
    case "string":
      return variable === variable.toUpperCase() ? `${variable}` : `"${variable}"`
    default:
      return variable
  }
}
const tab = (n: number) => new Array(n + 1).fill("").join("\t")
const t = tab
const replaceLast = (str: string, pattern: string | RegExp, replacement: string) => {
  const match = typeof pattern === "string"
    ? pattern
    : (str.match(new RegExp(pattern.source, "g")) || []).slice(-1)[0]
  if (!match) return str
  const last = str.lastIndexOf(match)
  return last !== -1 ? `${str.slice(0, last)}${replacement}${str.slice(last + match.length)}` : str
}

export const operationParser: OperationParser = (root: string) => {
  let operation = `${root} {\n\t|${root}|\n}`
  const fields: string[] = [root]

  return {
    get() {
      return operation.replace(/(\n|,)?(\t+| )\|\w+\b\|/g, "")
    },
    set({ level, subField, alias, hasSubField, isUnion, variables = "" }) {
      fields.splice(level, 0, subField)
      while (fields.length - 1 > level) fields.pop()

      if (variables) {
        const convertedType: [string[], string[]] = [[], []]
        const variable = Object.entries(variables)
        for (let i = 0; i <= variable.length - 1; i++) {
          convertedType[0].push(`${variable[i][0]}: ${rewriteVarValues(variable[i][1])}`)
        }
        variables = ` (${convertedType[0].join(", ")})`
      }

      const union = isUnion ? "... on " : ""
      alias = alias ? `${alias}: ` : ""
      subField = hasSubField ? `${` {\n${t(level + 1)}|${fields.at(-1)}|\n${t(level)}}`}` : ""

      operation = replaceLast(
        operation,
        `|${fields[level - 1]}|`,
        // RegExp("%" + fields[level - 1] + "(?<!\n)", ""),
        `${union}${alias}${fields[level]}${variables}${subField}\n${t(level)}|${
          fields[level - 1]
        }|`,
      )

      return this
    },
  }
}

export const getFieldName = (name: string) =>
  name.startsWith("with") ? toCase("down", name.slice(4)) : name

const fetch: Fetch = async (init) => {
  let host:
    | "https://graphql.anilist.co"
    | "https://anilist.co/api/v2/oauth/token"

  const body = {}
  const headers = new Headers({
    "Content-Type": "application/json",
    "Accept": "application/json",
  })
  if ("code" in init) {
    const {
      client_id,
      client_secret,
      code,
      redirect_uri,
      grant_type = "authorization_code",
    } = init
    host = "https://anilist.co/api/v2/oauth/token"
    Object.assign(body, {
      grant_type,
      client_id,
      client_secret,
      redirect_uri,
      code,
    })
  } else {
    host = "https://graphql.anilist.co"
    const { query, token } = init
    Object.assign(body, { query })
    if (token) headers.set("Authorization", `Bearer ${token}`)
  }

  const res = await self.fetch(host, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
  })

  if (res.status !== 200) {
    if ("code" in init) throw new AuthorizationError(await res.json())
    throw new ResponseError(await res.json())
  }
  return await res.json()
}

export const FuzzyDate = ({ operation, level }: Fields<OperationParser>) => ({
  day(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "day", level })
    return this
  },
  month(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "month", level })
    return this
  },
  year(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "year", level })
    return this
  },
})

export const Deleted = ({ operation, level }: Fields<OperationParser>) => ({
  /** If an item has been successfully deleted */
  deleted(op?: { alias?: string }) {
    operation.set({ alias: op?.alias, subField: "deleted", level })
    console.log("asd")
    return this
  },
})

export const Client = function (auth?: { token: string }) {
  let operation: ReturnType<OperationParser>

  return {
    get auth() {
      return {
        getToken(init: {
          code: string
          client_id: string
          client_secret: string
          redirect_uri: string
        }) {
          return fetch(init)
        },
      }
    },
    get query() {
      operation = operationParser("query")

      return {
        get raw() {
          return {
            get() {
              return operation.get()
            },
          }
        },
        fetch() {
          return fetch({ query: this.raw.get()! })
        },
        Page(op: { alias?: string; args?: AtLeastOne<QueryPageArgs>; fn: Fn<typeof Page> }) {
          operation.set({
            subField: "Page",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          op.fn(Page({ operation, level: 2 }))
          return this
        },
        /** Media query */
        Media(op: { alias?: string; args: AtLeastOne<QueryMediaArgs>; fn?: Fn<typeof Media> }) {
          operation.set({
            subField: getFieldName(this.Media.name),
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Media({ operation, level: 2 }))
          else Media({ operation, level: 2 }).id()
          return this
        },
        /** Media Trend query */
        MediaTrend(
          op: { alias?: string; args: AtLeastOne<QueryMediaTrendArgs>; fn?: Fn<typeof MediaTrend> },
        ) {
          operation.set({
            subField: "MediaTrend",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(MediaTrend({ operation, level: 2 }))
          else MediaTrend({ operation, level: 2 }).mediaId()
          return this
        },
        /** Airing schedule query */
        AiringSchedule(
          op: {
            alias?: string
            args: AtLeastOne<QueryAiringScheduleArgs>
            fn?: Fn<typeof AiringSchedule>
          },
        ) {
          operation.set({
            subField: "AiringSchedule",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(AiringSchedule({ operation, level: 2 }))
          else AiringSchedule({ operation, level: 2 }).id()
          return this
        },
        /** Character query */
        Character(
          op: { alias?: string; args: AtLeastOne<QueryCharacterArgs>; fn?: Fn<typeof Character> },
        ) {
          operation.set({
            subField: "Character",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Character({ operation, level: 2 }))
          else Character({ operation, level: 2 }).id()
          return this
        },
        /** Staff query */
        Staff(op: { alias?: string; args: AtLeastOne<QueryStaffArgs>; fn?: Fn<typeof Staff> }) {
          operation.set({
            subField: "Staff",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Staff({ operation, level: 2 }))
          else Staff({ operation, level: 2 }).id()
          return this
        },
        /** Media list query */
        MediaList(
          op: { alias?: string; args: AtLeastOne<QueryMediaListArgs>; fn?: Fn<typeof MediaList> },
        ) {
          operation.set({
            subField: "MediaList",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(MediaList({ operation, level: 2 }))
          else MediaList({ operation, level: 2 }).id()
          return this
        },
        /** Media list collection query, provides list pre-grouped by status & custom lists. User ID and Media Type arguments required. */
        MediaListCollection(
          op: {
            alias?: string
            args: AtLeastOne<QueryMediaListCollectionArgs>
            fn?: Fn<typeof MediaListCollection>
          },
        ) {
          operation.set({
            subField: "MediaListCollection",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(MediaListCollection({ operation, level: 2 }))
          else MediaListCollection({ operation, level: 2 }).user()
          return this
        },
        /** Collection of all the possible media genres */
        GenreCollection() {
          operation.set({ subField: "GenreCollection", level: 1 })
          return this
        },
        /** Collection of all the possible media tags */
        MediaTagCollection(
          op?: { alias?: string; args?: QueryMediaTagCollectionArgs; fn?: Fn<typeof MediaTag> },
        ) {
          operation.set({
            subField: "MediaTagCollection",
            variables: op?.args,
            hasSubField: true,
            alias: op?.alias,
            level: 1,
          })
          if (op?.fn) op.fn(MediaTag({ operation, level: 2 }))
          else MediaTag({ operation, level: 2 }).id()
          return this
        },
        /** User query */
        User(op: { alias?: string; args: AtLeastOne<QueryUserArgs>; fn?: Fn<typeof User> }) {
          operation.set({
            subField: "User",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Get the currently authenticated user */
        Viewer(op: { alias?: string; fn?: Fn<typeof User> }) {
          operation.set({
            subField: "Viewer",
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Notification query */
        Notification(
          op: {
            alias?: string
            args: AtLeastOne<QueryNotificationArgs>
            fn: Fn<typeof NotificationUnion>
          },
        ) {
          operation.set({
            subField: "Notification",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          op.fn(NotificationUnion({ operation, level: 2 }))
          return this
        },
        /** Studio query */
        Studio(op: { alias?: string; args: AtLeastOne<QueryStudioArgs>; fn?: Fn<typeof Studio> }) {
          operation.set({
            subField: "Studio",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Studio({ operation, level: 2 }))
          else Studio({ operation, level: 2 }).id()
          return this
        },
        /** Review query */
        Review(op: { alias?: string; args: AtLeastOne<QueryReviewArgs>; fn?: Fn<typeof Review> }) {
          operation.set({
            subField: "Review",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Review({ operation, level: 2 }))
          else Review({ operation, level: 2 }).id()
          return this
        },
        /** Activity query */
        Activity(
          op: {
            alias?: string
            args: AtLeastOne<QueryActivityArgs>
            fn?: Fn<typeof ActivityUnion>
          },
        ) {
          operation.set({
            alias: op.alias,
            subField: "Activity",
            variables: op.args,
            hasSubField: true,
            level: 1,
          })
          if (op.fn) op.fn(ActivityUnion({ operation, level: 2 }))
          else {
            ActivityUnion({ operation, level: 2 })
              .listActivity()
              .messageActivity()
              .textActivity()
          }
          return this
        },
        /** Activity reply query */
        ActivityReply(
          op: {
            alias?: string
            args: AtLeastOne<QueryActivityReplyArgs>
            fn?: Fn<typeof ActivityReply>
          },
        ) {
          operation.set({
            subField: "ActivityReply",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(ActivityReply({ operation, level: 2 }))
          else ActivityReply({ operation, level: 2 }).id()
          return this
        },
        /** Follow query */
        Following(
          op: { alias?: string; args: AtLeastOne<QueryFollowingArgs>; fn?: Fn<typeof User> },
        ) {
          operation.set({
            subField: "Following",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Follow query */
        Follower(
          op: { alias?: string; args: AtLeastOne<QueryFollowerArgs>; fn?: Fn<typeof User> },
        ) {
          operation.set({
            subField: "Follower",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Thread query */
        Thread(op: { alias?: string; args: AtLeastOne<QueryThreadArgs>; fn?: Fn<typeof Thread> }) {
          operation.set({
            subField: "Thread",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Thread({ operation, level: 2 }))
          else Thread({ operation, level: 2 }).id()
          return this
        },
        /** Comment query */
        ThreadComment(
          op: {
            alias?: string
            args: AtLeastOne<QueryThreadCommentArgs>
            fn?: Fn<typeof ThreadComment>
          },
        ) {
          operation.set({
            level: 1,
            subField: "ThreadComment",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(ThreadComment({ operation, level: 2 }))
          else ThreadComment({ operation, level: 2 }).id()
          return this
        },
        /** Recommendation query */
        Recommendation(
          op: {
            alias?: string
            args: AtLeastOne<QueryRecommendationArgs>
            fn?: Fn<typeof Recommendation>
          },
        ) {
          operation.set({
            subField: "Recommendation",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(Recommendation({ operation, level: 2 }))
          else Recommendation({ operation, level: 2 }).id()
          return this
        },
        /** Like query */
        Like(op: { alias?: string; args: AtLeastOne<QueryLikeArgs>; fn?: Fn<typeof User> }) {
          operation.set({
            subField: "Like",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Provide AniList markdown to be converted to html (Requires auth) */
        Markdown(op: { alias?: string; args: QueryMarkdownArgs; fn?: Fn<typeof Markdown> }) {
          operation.set({
            subField: "Markdown",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
            level: 1,
          })
          if (op.fn) op.fn(Markdown({ operation, level: 2 }))
          else Markdown({ operation, level: 2 }).html()
          return this
        },
        AniChartUser(op?: { alias?: string; fn?: Fn<typeof AniChartUser> }) {
          operation.set({ alias: op?.alias, subField: "AniChartUser", hasSubField: true, level: 1 })
          if (op?.fn) op.fn(AniChartUser({ operation, level: 2 }))
          else AniChartUser({ operation, level: 2 }).user()
          return this
        },
        /** Site statistics query */
        SiteStatistics(op: { alias?: string; fn: Fn<typeof SiteStatistics> }) {
          operation.set({
            subField: "SiteStatistics",
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          op.fn(SiteStatistics({ operation, level: 2 }))
          return this
        },
        /** ExternalLinkSource collection query */
        ExternalLinkSourceCollection(
          op: {
            alias?: string
            args: AtLeastOne<QueryExternalLinkSourceCollectionArgs>
            fn?: Fn<typeof ExternalLinkSourceCollection>
          },
        ) {
          operation.set({
            subField: "ExternalLinkSourceCollection",
            variables: op.args,
            hasSubField: true,
            alias: op.alias,
            level: 1,
          })
          if (op.fn) op.fn(ExternalLinkSourceCollection({ operation, level: 2 }))
          else ExternalLinkSourceCollection({ operation, level: 2 }).id()
          return this
        },
      }
    },
    get mutation() {
      operation = operationParser(
        "mutation",
      )

      return {
        get raw() {
          return {
            get() {
              return operation.get()
            },
          }
        },
        fetch() {
          return fetch({
            query: this.raw.get()!,
            token: auth!.token,
          })
        },
        /** Update current user options */
        UpdateUser(op: { alias?: string; args: MutationUpdateUserArgs; fn?: Fn<typeof User> }) {
          operation.set({
            level: 1,
            subField: "UpdateUser",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Create or update a media list entry */
        SaveMediaListEntry(
          op: { alias?: string; args: MutationSaveMediaListEntryArgs; fn?: Fn<typeof MediaList> },
        ) {
          operation.set({
            level: 1,
            subField: "SaveMediaListEntry",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(MediaList({ operation, level: 2 }))
          else MediaList({ operation, level: 2 }).id()
          return this
        },
        /** Update multiple media list entries to the same values */
        UpdateMediaListEntries(
          op: {
            alias?: string
            args: MutationUpdateMediaListEntriesArgs
            fn?: Fn<typeof MediaList>
          },
        ) {
          operation.set({
            level: 1,
            subField: "UpdateMediaListEntries",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(MediaList({ operation, level: 2 }))
          else MediaList({ operation, level: 2 }).id()
          return this
        },
        /** Delete a media list entry */
        DeleteMediaListEntry(op: { alias?: string; args: MutationDeleteMediaListEntryArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteMediaListEntry",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        /** Delete a custom list and remove the list entries from it */
        DeleteCustomList(op: { alias?: string; args: MutationDeleteCustomListArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteCustomList",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        /** Create or update text activity for the currently authenticated user */
        SaveTextActivity(
          op: { alias?: string; args: MutationSaveTextActivityArgs; fn?: Fn<typeof TextActivity> },
        ) {
          operation.set({
            level: 1,
            subField: "TextActivity",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(TextActivity({ operation, level: 2 }))
          else TextActivity({ operation, level: 2 }).id()
          return this
        },
        /** Create or update message activity for the currently authenticated user */
        SaveMessageActivity(
          op: {
            alias?: string
            args: MutationSaveMessageActivityArgs
            fn?: Fn<typeof MessageActivity>
          },
        ) {
          operation.set({
            level: 1,
            subField: "MessageActivity",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(MessageActivity({ operation, level: 2 }))
          else MessageActivity({ operation, level: 2 }).id()
          return this
        },
        /** Update list activity (Mod Only) */
        SaveListActivity(
          op: { alias?: string; args: MutationSaveListActivityArgs; fn?: Fn<typeof ListActivity> },
        ) {
          operation.set({
            level: 1,
            subField: "ListActivity",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(ListActivity({ operation, level: 2 }))
          else ListActivity({ operation, level: 2 }).id()
          return this
        },
        /** Delete an activity item of the authenticated users */
        DeleteActivity(op: { alias?: string; args: MutationDeleteActivityArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteActivity",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        /** Toggle activity to be pinned to the top of the user's activity feed */
        ToggleActivityPin(
          op: { alias?: string; args: MutationToggleActivityPinArgs; fn: Fn<typeof ActivityUnion> },
        ) {
          operation.set({
            level: 1,
            subField: "ToggleActivityPin",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          op.fn(ActivityUnion({ operation, level: 2 }))
          return this
        },
        /** Toggle the subscription of an activity item */
        ToggleActivitySubscription(
          op: {
            alias?: string
            args: MutationToggleActivitySubscriptionArgs
            fn: Fn<typeof ActivityUnion>
          },
        ) {
          operation.set({
            level: 1,
            subField: "ToggleActivitySubscription",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          op.fn(ActivityUnion({ operation, level: 2 }))
          return this
        },
        /** Create or update an activity reply */
        SaveActivityReply(
          op: {
            alias?: string
            args: MutationSaveActivityReplyArgs
            fn?: Fn<typeof ActivityReply>
          },
        ) {
          operation.set({
            level: 1,
            subField: "SaveActivityReply",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(ActivityReply({ operation, level: 2 }))
          else ActivityReply({ operation, level: 2 }).id()
          return this
        },
        /** Delete an activity reply of the authenticated users */
        DeleteActivityReply(op: { alias?: string; args: MutationDeleteActivityReplyArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteActivityReply",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        /** Add or remove a like from a likeable type. Returns all the users who liked the same model */
        ToggleLike(op: { alias?: string; args: MutationToggleLikeArgs; fn?: Fn<typeof User> }) {
          operation.set({
            level: 1,
            subField: "ToggleLike",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Add or remove a like from a likeable type. */
        ToggleLikeV2(
          op: { alias?: string; args: MutationToggleLikeV2Args; fn: Fn<typeof LikeableUnion> },
        ) {
          operation.set({
            level: 1,
            subField: "ToggleLikeV2",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          op.fn(LikeableUnion({ operation, level: 2 }))
          return this
        },
        /** Toggle the un/following of a user */
        ToggleFollow(op: { alias?: string; args: MutationToggleFollowArgs; fn?: Fn<typeof User> }) {
          operation.set({
            level: 1,
            subField: "ToggleFollow",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(User({ operation, level: 2 }))
          else User({ operation, level: 2 }).id()
          return this
        },
        /** Favourite or unfavourite an anime, manga, character, staff member, or studio */
        ToggleFavourite(
          op: { alias?: string; args: MutationToggleFavouriteArgs; fn: Fn<typeof Favourites> },
        ) {
          operation.set({
            level: 1,
            subField: "ToggleFavourite",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          op.fn(Favourites({ operation, level: 2 }))
          return this
        },
        /** Update the order favourites are displayed in */
        UpdateFavouriteOrder(
          op: { alias?: string; args: MutationUpdateFavouriteOrderArgs; fn: Fn<typeof Favourites> },
        ) {
          operation.set({
            level: 1,
            subField: "UpdateFavouriteOrder",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          op.fn(Favourites({ operation, level: 2 }))
          return this
        },
        /** Create or update a review */
        SaveReview(op: { alias?: string; args: MutationSaveReviewArgs; fn?: Fn<typeof Review> }) {
          operation.set({
            level: 1,
            subField: "SaveReview",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(Review({ operation, level: 2 }))
          else Review({ operation, level: 2 }).id()
          return this
        },
        /** Delete a review */
        DeleteReview(op: { alias?: string; args: MutationDeleteReviewArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteReview",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        /** Rate a review */
        RateReview(op: { alias?: string; args: MutationRateReviewArgs; fn?: Fn<typeof Review> }) {
          operation.set({
            level: 1,
            subField: "RateReview",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(Review({ operation, level: 2 }))
          else Review({ operation, level: 2 }).id()
          return this
        },
        /** Recommendation a media */
        SaveRecommendation(
          op: {
            alias?: string
            args: MutationSaveRecommendationArgs
            fn?: Fn<typeof Recommendation>
          },
        ) {
          operation.set({
            level: 1,
            subField: "SaveRecommendation",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(Recommendation({ operation, level: 2 }))
          else Recommendation({ operation, level: 2 }).id()
          return this
        },
        /** Create or update a forum thread */
        SaveThread(op: { alias?: string; args: MutationSaveThreadArgs; fn?: Fn<typeof Thread> }) {
          operation.set({
            level: 1,
            subField: "SaveThread",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(Thread({ operation, level: 2 }))
          else Thread({ operation, level: 2 }).id()
          return this
        },
        /** Delete a thread */
        DeleteThread(op: { alias?: string; args: MutationDeleteThreadArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteThread",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        /** Toggle the subscription of a forum thread */
        ToggleThreadSubscription(
          op: {
            alias?: string
            args: MutationToggleThreadSubscriptionArgs
            fn?: Fn<typeof Thread>
          },
        ) {
          operation.set({
            level: 1,
            subField: "ToggleThreadSubscription",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(Thread({ operation, level: 2 }))
          else Thread({ operation, level: 2 }).id()
          return this
        },
        /** Create or update a thread comment */
        SaveThreadComment(
          op: {
            alias?: string
            args: MutationSaveThreadCommentArgs
            fn?: Fn<typeof ThreadComment>
          },
        ) {
          operation.set({
            level: 1,
            subField: "SaveThreadComment",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          if (op.fn) op.fn(ThreadComment({ operation, level: 2 }))
          else ThreadComment({ operation, level: 2 }).id()
          return this
        },
        /** Delete a thread comment */
        DeleteThreadComment(op: { alias?: string; args: MutationDeleteThreadCommentArgs }) {
          operation.set({
            level: 1,
            subField: "DeleteThreadComment",
            hasSubField: true,
            alias: op.alias,
            variables: op.args,
          })
          Deleted({ operation, level: 2 }).deleted()
          return this
        },
        UpdateAniChartSettings() {
          throw "To be Implemented"
        },
        UpdateAniChartHighlights() {
          throw "To be Implemented"
        },
      }
    },
  }
}
