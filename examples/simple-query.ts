import { Client } from "../mod.ts"
import { MediaType } from "../src/types/generated/graphql.ts"

// const query = anilist.query.media("Kamisama Ni Natta Hi").id().titles().genres();
const query = Client().query.Media(
  { type: MediaType.Anime, search: "Kamisama Ni Natta Hi" },
  (media) =>
    media
      .id()
      .title((title) => title.userPreferred())
      .genres(),
)
console.log(query.raw.get()) /*
query {
    Media(type: "ANIME", search: "Kamisama Ni Natta Hi") {
        id,
        title {
            romaji
        },
        genres
    }
}
*/

const data = await query.fetch()

console.log(data)
/*
{
  data: {
    Media: {
      id: 118419,
      title: { userPreferred: "Kamisama ni Natta Hi" },
      genres: [ "Comedy", "Drama", "Romance", "Sci-Fi", "Slice of Life" ]
    }
  }
}
  */
