import { Client } from "../mod.ts"

const page = Client().query.Page(
  { perPage: 1 },
  (page) =>
    page.media(
      { search: "Kamisama Ni Natta" },
      (media) =>
        media.title(
          (title) => title.userPreferred(),
        ),
    ),
)

console.log(await page.fetch())
