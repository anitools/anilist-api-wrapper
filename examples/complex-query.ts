import { Client } from "../mod.ts"

const query = Client().query.Media({ id: 1 }, (media) =>
  media
    .id()
    .idMal()
    .title((title) => title.userPreferred())
    .type()
    .format()
    .status()
    .description()
    .startDate((start) => start.year().month().day())
    .endDate((end) => end.year().month().day())
    .season()
    .seasonYear()
    .seasonInt()
    .episodes()
    .duration()
    .chapters()
    .volumes()
    .countryOfOrigin()
    .isLicensed()
    .source()
    .hashtag()
    .trailer((trailer) => trailer.id())
    .updatedAt()
    .coverImage((cover) => cover.medium())
    .bannerImage()
    .genres()
    .synonyms()
    .averageScore()
    .meanScore()
    .popularity()
    .isLocked()
    .trending()
    .favourites()
    .tags((tags) => tags.id())
    .relations((relations) =>
      relations
        .edges((e) => e.node((n) => n.id()))
        .nodes((n) => n.id())
        .pageInfo((p) => p.total())
    )
    .characters(undefined, (character) =>
      character
        .edges((e) => e.node((n) => n.id()))
        .nodes((n) => n.id())
        .pageInfo((p) => p.total()))
    .staff(undefined, (staff) =>
      staff
        .edges((e) => e.node((n) => n.id()))
        .nodes((n) => n.id())
        .pageInfo((p) => p.total()))
    .studios(undefined, (studio) =>
      studio
        .edges((e) => e.node((n) => n.id()))
        .nodes((n) => n.id())
        .pageInfo((p) => p.total()))
    .isFavourite()
    .isFavouriteBlocked()
    .isAdult()
    .nextAiringEpisode((ep) => ep.airingAt())
    .externalLinks((links) => links.id())
    .streamingEpisodes((ep) => ep.site())
    .rankings((rank) => rank.rank())
    .mediaListEntry((entry) => entry.id()))

console.log(await query.fetch())
