import { Client } from "../mod.ts"
import { MediaType } from "../src/types/generated/graphql.ts"

const query = Client().query.Page(
  { perPage: 1 },
  (page) =>
    page.media(
      { type: MediaType.Anime },
      (media) =>
        media.title(
          (title) => title.userPreferred(),
        ).id().description(),
    ),
)

console.log(query.raw.get()) /*
query {
    Page(perPage: 1) {
        media(type: ANIME) {
            title {
                userPreferred
            },
            id,
            description
        }
    }
}
*/

const data = await query.fetch()

console.log(data)
/*
{
    media: [
        {
            title: { romaji: 'Cowboy Bebop' },
            id: 1,
            description: 'Enter a world in the distant future, where Bounty Hunters roam the solar system. Spike and Jet, bounty hunting partners, set out on journeys in an ever struggling effort to win bounty rewards to survive.<br><br>\n' +
                'While traveling, they meet up with other very interesting people. Could Faye, the beautiful and ridiculously poor gambler, Edward, the computer genius, and Ein, the engineered dog be a good addition to the group?'
        }
    ]
}
*/
