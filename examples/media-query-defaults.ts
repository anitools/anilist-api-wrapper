import { Client } from "../mod.ts"
import { MediaType } from "../src/types/generated/graphql.ts"

const query = Client().query.Media({ type: MediaType.Anime, search: "Kamisama Ni Natta Hi" })

console.log(query.raw.get())
/*
query {
    Media(type: ANIME, search: "Kamisama Ni Natta Hi") {
        id
    }
}
*/

console.log(await query.fetch())
/*
{
    data: {
        Media: {
            id: 118419,
        }
    }
}
*/
