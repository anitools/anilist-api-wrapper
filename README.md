# Anilist Wrapper

An UNOFFICIAL wrapper for the anilist api written in typescript.

You can visit the official graphql docs for anilist
[here](https://anilist.github.io/ApiV2-GraphQL-Docs/) to find out everything you can do[^*].

This wrapper is still in an early state. Interface may change.

## Table of Contents

- [Anilist Wrapper](#anilist-wrapper)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
  - [Usage](#usage)
    - [Creating a query](#creating-a-query)
    - [Query arguments](#query-arguments)
    - [Creating the query](#creating-the-query)
      - [Other Queries](#other-queries)
        - [Fetching without building the query](#fetching-without-building-the-query)
        - [Creating a complete search query](#creating-a-complete-search-query)
  - [OAuth](#oauth)
    - [Using Token](#using-token)
    - [Getting a token from an authorization code grant](#getting-a-token-from-an-authorization-code-grant)

## Installation

```
import { Client } from "https://deno.land/x/anilist@v0.7.5/mod.ts"
```

## Usage

### Creating a query

```js
const query = Client().query.MediaList()
```

### Query arguments

The queries can accept either an object of `MediaListArguments`.

```js
const queryByUserName = Client().query.MediaList({ userName: "Josh" })
/*
query ($id: Int) {
    MediaList(id: $id) {
        id
    }
}
*/

const queryById = Client().query.MediaList({ id: 1 })
/*
query ($userName: String) {
    MediaList(userName: $userName) {
        id
    }
}
*/
```

### Creating the query

#### Other Queries

##### Fetching without building the query

If you build the query and try to fetch it without telling which fields to return it will default to
returning `id` to avoid errors.

```js
const query = Client().query.MediaList({ userName: "Josh" })

await query.fetch()
```

=>

```json
{
  "data": {
    "MediaList": {
      "id": 5318
    }
  }
}
```

##### Creating a complete search query

As the library follows the builder pattern you can just nest functions until you have every field
you want.

```js
const query = Client().query.MediaList({ userName: "Josh" }, (fields) => {
  fields.id()
  fields.userId()
    .private()
    .startedAt((fields) => fields.year().month().day())
    .completedAt((fields) => fields.year().month().day())
})

await query.fetch()
```

=>

```json
{
  "data": {
    "MediaList": {
      "id": 5318,
      "userId": 1,
      "private": false,
      "startedAt": { "year": null, "month": null, "day": null },
      "completedAt": { "year": 2017, "month": 11, "day": 24 }
    }
  }
}
```

## OAuth

### Using Token

Once you have a token, you can pass it into `Client({ token })`.

### Getting a token from an authorization code grant

This method allows you to convert the Authorization Code Grant into an access token. The response is
a type `Authorization`.

```ts
const token = Client().auth.getToken({
  client_id: "<the id as a string>",
  client_secret: "<the client secret>",
  code: "<code>",
  redirect_uri: "<the redirect uri, this is required>",
})
```

=>

```jsonc
{
  "token_type": "string",
  /* A full year in seconds */
  "expires_in": "number",
  "access_token": "string",
  /* Currently not used for anything */
  "refresh_token": "string"
}
```

[^*]: Not everything is supported yet, please refer to the todo list to see what has full
implementation or open an issue to talk about it
