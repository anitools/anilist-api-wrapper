import { type CodegenConfig, generate } from "npm:@graphql-codegen/cli"
import "npm:@graphql-codegen/typescript"
import { parse } from "https://deno.land/std@0.204.0/path/mod.ts"

const meta = parse(new URL(import.meta.url).pathname)

const config: CodegenConfig = {
  overwrite: true,
  schema: "https://graphql.anilist.co",
  verbose: true,
  debug: true,
  generates: {
    [`${meta.dir}/generated/graphql.ts`]: {
      plugins: ["typescript"],
      config: {
        useImplementingTypes: true,
        avoidOptionals: true,
        scalars: {
          CountryCode: {
            input: "string",
            output: "string",
          },
          FuzzyDateInt: {
            input: "string",
            output: "string",
          },
          Json: {
            input: "Array<Record<string, unknown>>",
            output: "Array<Record<string, unknown>>",
          },
        },
      },
    },
    // "schema.graphql": {
    //   plugins: ["schema-ast"],
    // },
  },
}

generate(config)
